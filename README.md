# TS Anti-Virus

TS Anti-Virus is a program that scans files statically and dynamically in order to detect, prevent and protect your computer from malwares.


It was developed by Tzur Goldstein and Shiraz Ben Haim.

![image](https://drive.google.com/uc?export=view&id=1hV9w4P90tBx-f8gW-18hFVMid2Uaylv-)


## Features

  * Hooking protection.
  * Prevent privilege escalation.
  * Yara rules scan.
  * Hash scan using virus total.
  * Prevent rootkits install.
  * Hash scan using our DB.
  * Verifying file signatures.
  * Prevent backup logs and system images from being deleted.
  * Detect attempts of malwares to disable security programs.
  * Statistics panel.
  * Detailed scan report. 
  * melt file detection.
  * Beautiful GUI.
  * Drag & Drop option.

&nbsp;

## Scans

### Static Scan

  A scan that performed without running the suspicious file.
  Our static scan includes hash scan using virus total and our DB, yara 
  rules scan, verifying file signatures and finding suspicious strings in 
  the scanned file using strings.exe.

  <br/>
  ![gif](https://drive.google.com/uc?export=view&id=1aaIoqRliPPq6OWA76_mOspz5SqnzqvfT)
  <br/>
	
### Dynamic Scan

  A scan that runs the file and concludes according to its behavior while 
  running whether it is malicious or safe.
  Our dynamic scan includes hooking, registry & startup monitoring, 
  prevent privileges escalation, check whether the suspicious file checks 
  if it is run in sandbox/debugger, detect melt files, prevent backup 
  logs and system images from being deleted, prevent rootkits install and 
  detect attempts of malwares to disable security programs. 

  <br/>
  ![gif](https://drive.google.com/uc?export=view&id=1tcPZW3NmaX6pOMN07weOF5p0AIUn9_of)
  <br/>

### Static & Dynamic scan
  
  <br/>
  ![gif](https://drive.google.com/uc?export=view&id=1Ww_bLRtnlLFWsCUGae4Gb5X7ZVf5Dsms)
  <br/>

## Demo

### Full Demo(Video clip- add link)


## Installation

Firstly, download the client and the server.
<br/>
Secondly, download VMware Workstation Player.
<br/>
<b>Note:</b> you need to install everything on C driver.
<br/>
After that, you can run TS Anti-Virus.


## Contributing
Pull requests are welcome. For any ideas/thoughts, please open an issue to discuss about it. If you need support feel free to contact us.

Please make sure to update tests as appropriate.

## Requirements
  * Windows 10 64 bit OS.
  * Administrative privileges.
  * VMware Workstation Player installed.
  * good vibes :)

### Enjoy!
