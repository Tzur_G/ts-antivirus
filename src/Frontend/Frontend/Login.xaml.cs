﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Frontend.TsClient;
using Frontend.TsClient.Requests;
using Frontend.TsClient.Responses;
using Newtonsoft.Json;

namespace Frontend
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        private Communicator m_client;

        public Login(Communicator client)
        {
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen; // put this window in the middle of screen
            InitializeComponent();
            this.m_client = client;
            this.usernameBox.Focus();
        }

        private void ShowPasswordCharsCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            passTextBox.Text = passBox.Password;
            passBox.Visibility = Visibility.Collapsed;
            passTextBox.Visibility = Visibility.Visible;
        }

        private void ShowPasswordCharsCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            passBox.Password = passTextBox.Text;
            passTextBox.Visibility = Visibility.Collapsed;
            passBox.Visibility = Visibility.Visible;
        }

        private void VerifyAccount_Clicked(object sender, RoutedEventArgs e)
        {
            this.Close();
            RecoverAccount recoverAccount = new RecoverAccount(this.m_client);
            recoverAccount.ShowDialog();
        }

        private void LoginButton_Clicked(object sender, RoutedEventArgs e)
        {
            usernameBox.BorderBrush = Brushes.White;
            passBox.BorderBrush = Brushes.White;
            passTextBox.BorderBrush = Brushes.White;
            string password = "";
            if ((bool)this.showPassCharsCheckBox.IsChecked)
                password = this.passTextBox.Text;
            else
                password = this.passBox.Password;

            this.passTextBox.Text = password;
            if (!UserValidation.AreNamePassFulled(this.usernameBox, this.passTextBox, this.passBox))
            // check if there ate no empty fields
            {
                this.Warning.Visibility = Visibility.Visible;
                this.ErrorLable.Content = "Fill all Fields";
            }
            else if (!UserValidation.IsUsernameValid(this.usernameBox.Text))
            // check if username is valid
            {
                this.usernameBox.BorderBrush = Brushes.Red; // show warnning
                this.Warning.Visibility = Visibility.Visible;
                this.ErrorLable.Content = "user doesn't exist";
            }
            else if (!UserValidation.IsPasswordValid(password))
            // check if password is valid
            {
                this.passBox.BorderBrush = Brushes.Red; // show warnning
                this.passTextBox.BorderBrush = Brushes.Red; // show warnning
                this.Warning.Visibility = Visibility.Visible;
                this.ErrorLable.Content = "Password is incorrect";
            }
            else
            {
                // Send request
                LoginRequest loginRequest = new LoginRequest { Username = this.usernameBox.Text, Password = password};
                GeneralResponse generalResponse = this.m_client.
                    SendAPIRequest<LoginRequest>((int)PacketTypes.LOGIN, loginRequest);

                if (generalResponse.Op == (int)PacketTypes.LOGIN)
                {
                    LoginResponse loginResponse = JsonConvert.DeserializeObject<LoginResponse>(generalResponse.Buffer);
                    if (loginResponse.Status == 1)
                    {
                        this.m_client.Username = this.usernameBox.Text;
                        this.m_client.Password = password;

                        // Open menu
                        this.Close();
                        Menu menuWindow = new Menu(this.m_client);
                        menuWindow.ShowDialog();
                    }
                    else 
                    {
                        this.Warning.Visibility = Visibility.Visible;
                        this.ErrorLable.Content = "Cannot login";
                    }
                }
                else // if this is an error response
                {
                    ErrorResponse err = JsonConvert.DeserializeObject<ErrorResponse>(generalResponse.Buffer);
                    this.Warning.Visibility = Visibility.Visible;
                    this.ErrorLable.Content = err.Message;
                }
            }
        }

        // Go to signup window
        private void SignupButton_Clicked(object sender, MouseButtonEventArgs e)
        {
            this.Close();
            // Open the signup window
            SignUp signup = new SignUp(this.m_client);
            signup.ShowDialog();
        }
    }


}
