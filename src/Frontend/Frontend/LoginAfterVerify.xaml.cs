﻿using Frontend.TsClient;
using Frontend.TsClient.Requests;
using Frontend.TsClient.Responses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Frontend
{
    /// <summary>
    /// Interaction logic for LoginAfterVerify.xaml
    /// </summary>
    public partial class LoginAfterVerify : Window
    {
        private Communicator m_client;
        private string userMail;

        public LoginAfterVerify(Communicator client, string email)
        {
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen; // put this window in the middle of screen
            InitializeComponent();
            this.m_client = client;
            this.userMail = email;
        }

        private void ShowPasswordCharsCheckBox_Checked(object sender, RoutedEventArgs e)
        {
                passTextBox.Text = passBox.Password;
                passBox.Visibility = Visibility.Collapsed;
                passTextBox.Visibility = Visibility.Visible; 
        }

        private void ShowPasswordCharsCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
                passBox.Password = passTextBox.Text;
                passTextBox.Visibility = Visibility.Collapsed;
                passBox.Visibility = Visibility.Visible;
        }

        // remove ability to copy, paste and cut
        private void PasswordBox_PreviewExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            if (e.Command == ApplicationCommands.Copy ||
                e.Command == ApplicationCommands.Cut ||
                e.Command == ApplicationCommands.Paste)
            {
                e.Handled = true;
            }
        }

        private void LoginButton_Clicked(object sender, RoutedEventArgs e)
        {
            string password = "";
            if ((bool)this.showPassCharsCheckBox.IsChecked)
                password = this.passTextBox.Text;
            else
                password = this.passBox.Password;
            this.passTextBox.Text = password;

            passBox.BorderBrush = (password.Length == 0) ? Brushes.Red : Brushes.White;
            passTextBox.BorderBrush = (password.Length == 0) ? Brushes.Red : Brushes.White;

            string repeatedPassword = this.repeatedPassBox.Password;
            repeatedPassBox.BorderBrush = (repeatedPassword.Length == 0) ? Brushes.Red : Brushes.White;

            if ((password.Length == 0) || (repeatedPassword.Length == 0))
            // check if there ate no empty fields
            {               
                this.Warning.Visibility = Visibility.Visible;
                this.ErrorLable.Content = "Fill all Fields";
            }
            else if (!UserValidation.IsPasswordValid(password))
            // check if password is valid
            {
                this.passBox.BorderBrush = Brushes.Red; // show warnning
                this.passTextBox.BorderBrush = Brushes.Red; // show warnning
                this.Warning.Visibility = Visibility.Visible;
                this.ErrorLable.Content = "Password is not valid";
            }
            else if (password != repeatedPassword)
            // check if the password and the confirmed password are equal
            {
                this.Warning.Visibility = Visibility.Visible;
                this.ErrorLable.Content = "Password mismatch";
            }
            else
            {
                SignupRequest signupRequest = new SignupRequest { Username = this.m_client.Username, Password = password, 
                    Email = this.userMail, Reset = true };
                GeneralResponse generalResponse = this.m_client.
                    SendAPIRequest<SignupRequest>((int)PacketTypes.SIGNUP, signupRequest);

                if (generalResponse.Op == (int)PacketTypes.SIGNUP) // status = 1
                {
                    SignupResponse signupResponse = JsonConvert.DeserializeObject<SignupResponse>(generalResponse.Buffer);
                    if (signupResponse.Status == 1)
                    {
                        this.m_client.Password = password;
                        this.m_client.Email = this.userMail;

                        // Open menu
                        this.Close();
                        Menu menuWindow = new Menu(this.m_client);
                        menuWindow.ShowDialog();
                    }
                }
                else // if this is an error response
                {
                    ErrorResponse err = JsonConvert.DeserializeObject<ErrorResponse>(generalResponse.Buffer);
                    this.Warning.Visibility = Visibility.Visible;
                    this.ErrorLable.Content = err.Message;
                }
            }
        }

        // Go to Recover Account window
        private void Back_Clicked(object sender, RoutedEventArgs e)
        {
            this.Close();
            RecoverAccount recover = new RecoverAccount(this.m_client);
            recover.ShowDialog();
        }
    }
}
