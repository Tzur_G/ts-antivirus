﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using Frontend.TsClient;
using Frontend.TsClient.Requests;
using Frontend.TsClient.Responses;
using Newtonsoft.Json;

namespace Frontend
{
    class Logout
    {
        public static bool LogoutUser(Communicator client)
        {
            // Send logout request
            LogoutRequest logoutRequest = new LogoutRequest { };
            GeneralResponse generalResponse = client.
                    SendAPIRequest<LogoutRequest>((int)PacketTypes.LOGOUT, logoutRequest);

            if (generalResponse.Op == (int)PacketTypes.LOGOUT)
            {
                client.Username = "";
                client.Password = "";
                client.Email = "";
                return true;
            }
            else // if this is an error response
            {
                ErrorResponse err = JsonConvert.DeserializeObject<ErrorResponse>(generalResponse.Buffer);
                _ = new MessageBoxCustom(err.Message, "Error", MessageButtons.Ok).ShowDialog();
                return false;
            }  
        }
    }
}
