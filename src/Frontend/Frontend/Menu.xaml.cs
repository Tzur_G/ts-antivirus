﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Frontend.MenuPages;
using Frontend.TsClient;

namespace Frontend
{
    /// <summary>
    /// Interaction logic for Menu.xaml
    /// </summary>
    public partial class Menu : Window
    {
        private Communicator m_client;

        public Menu(Communicator client)
        {
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen; // put this window in the middle of screen
            InitializeComponent();
            this.m_client = client;
            this.usernameLabel.Content = client.Username;

            // Show the scan page as default
            BrushMenuButtons(this.scanButton);
            ChooseFilePage page = new ChooseFilePage(this.m_client);
            this.PageInMenu.Content = page;
        }

        public void BrushMenuButtons(Button selectedButton)
        {
            var bc = new BrushConverter();
            this.scanButton.Background = (Brush)bc.ConvertFrom("#12172B");
            this.historyButton.Background = (Brush)bc.ConvertFrom("#12172B");
            this.statisticsButton.Background = (Brush)bc.ConvertFrom("#12172B");
            this.encryptedFilesButton.Background = (Brush)bc.ConvertFrom("#12172B");
            this.aboutButton.Background = (Brush)bc.ConvertFrom("#12172B");
            selectedButton.Background = (Brush)bc.ConvertFrom("#1c2444");            
        }

        private void ScanButton_Clicked(object sender, RoutedEventArgs e)
        {
            this.UserOptionsGrid.Visibility = Visibility.Hidden;
            if (!this.m_client.isScanning)
            {
                BrushMenuButtons(this.scanButton);
                ChooseFilePage page = new ChooseFilePage(this.m_client);
                this.PageInMenu.Content = page;
            }
        }

        private void HistoryButton_Clicked(object sender, RoutedEventArgs e)
        {
            this.UserOptionsGrid.Visibility = Visibility.Hidden;
            if (this.m_client.QuitScan())
            {
                BrushMenuButtons(this.historyButton);
                HistoryPage page = new HistoryPage(this.m_client);
                this.PageInMenu.Content = page;
            }
        }

        private void StatisticsButton_Clicked(object sender, RoutedEventArgs e)
        {
            this.UserOptionsGrid.Visibility = Visibility.Hidden;
            if (this.m_client.QuitScan())
            {
                BrushMenuButtons(this.statisticsButton);
                StatisticsPage page = new StatisticsPage(this.m_client);
                this.PageInMenu.Content = page;
            }
        }

        private void EncryptedFilesButton_Clicked(object sender, RoutedEventArgs e)
        {
            this.UserOptionsGrid.Visibility = Visibility.Hidden;
            if (this.m_client.QuitScan())
            {
                BrushMenuButtons(this.encryptedFilesButton);
                EncryptedFilesPage page = new EncryptedFilesPage(this.m_client);
                this.PageInMenu.Content = page;
            }
        }

        private void AboutButton_Clicked(object sender, RoutedEventArgs e)
        {
            this.UserOptionsGrid.Visibility = Visibility.Hidden;
            if (this.m_client.QuitScan())
            {
                BrushMenuButtons(this.aboutButton);
                AboutPage page = new AboutPage(this.m_client);
                this.PageInMenu.Content = page;
            }
        }

        private void OpenUserOptions(object sender, RoutedEventArgs e)
        {
            if (this.UserOptionsGrid.Visibility == Visibility.Hidden)
                this.UserOptionsGrid.Visibility = Visibility.Visible;
            else
                this.UserOptionsGrid.Visibility = Visibility.Hidden;
        }

        private void LogoutButton_Clicked(object sender, RoutedEventArgs e)
        {
            this.UserOptionsGrid.Visibility = Visibility.Hidden;
            if (this.m_client.QuitScan())
            {
                bool? Result = new MessageBoxCustom("Are you sure you want to logout?", "Wait", MessageButtons.YesNo).ShowDialog();
                if (Result.Value)
                {
                    this.Close();
                    if (Logout.LogoutUser(this.m_client))
                    {
                        this.Close();
                        Login login = new Login(this.m_client);
                        login.ShowDialog();
                    }
                }
            }
        }

        private void SettingsButton_Clicked(object sender, RoutedEventArgs e)
        {
            this.UserOptionsGrid.Visibility = Visibility.Hidden;
            if (this.m_client.QuitScan())
            {
                UserSettingsPage page = new UserSettingsPage(this.m_client);
                this.PageInMenu.Content = page;
            }
        }

        private void OnWindowClick(object sender, MouseButtonEventArgs e)
        {
            if (!this.usernameLabel.IsMouseOver && !this.UserImage.IsMouseOver)
                this.UserOptionsGrid.Visibility = Visibility.Hidden;
        }

        // Called when the user press on the close button
        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (this.m_client.isScanning)
            {
                bool? Result = new MessageBoxCustom("Are you sure you want to quit?", "Scan is in progress", MessageButtons.YesNo).ShowDialog();
                if (!Result.Value)
                {
                    e.Cancel = true; // if user doesn't want to stop the scan, cancel closure
                }
            }
        }
    }
}
