﻿using Frontend.TsClient;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Frontend.MenuPages
{
    /// <summary>
    /// Interaction logic for ChooseFilePage.xaml
    /// </summary>
    public partial class ChooseFilePage : Page
    {
        private Communicator m_client;

        public ChooseFilePage(Communicator client)
        {
            InitializeComponent();
            this.m_client = client;
        }

        private void ChooseFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                ChooseScanPage page = new ChooseScanPage(this.m_client, openFileDialog.FileName);
                this.NavigationService.Content = page;
            }
        }

        private void InputFile_PreviewDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                // Note that you can have more than one file.
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                if ((files.Length > 1) || !files[0].Contains('.'))
                {                 
                    e.Effects = DragDropEffects.None;
                    e.Handled = true;
                    this.Warning.Visibility = Visibility.Visible;
                    this.ErrorLable.Content = "Drop only one file";
                }
                else
                {
                    this.DragPicture.Source = new BitmapImage(new Uri("/Pictures/DragAfter.png", UriKind.Relative));
                }
            }           
        }

        private new void PreviewDragLeave(object sender, DragEventArgs e)
        {
            this.DragPicture.Source = new BitmapImage(new Uri("/Pictures/DragBefore.png", UriKind.Relative));
        }

        private void InputFile_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                // Note that you can have more than one file.
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                var file = files[0];

                ChooseScanPage page = new ChooseScanPage(this.m_client, file);
                this.NavigationService.Content = page;
            }
        }
    }
}
