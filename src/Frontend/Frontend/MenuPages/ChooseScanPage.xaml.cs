﻿using Frontend.TsClient;
using Frontend.TsClient.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Frontend.MenuPages
{
    /// <summary>
    /// Interaction logic for ChooseScanPage.xaml
    /// </summary>
    public partial class ChooseScanPage : Page
    {
        private Communicator m_client;
        private string m_path;

        public ChooseScanPage(Communicator client, string path)
        {
            InitializeComponent();
            this.FilePathTextBlock.Text = path;
            this.m_client = client;
            this.m_path = path;
        }

        // Get back to the state when the user need to choose a file
        private void XButton_Clicked(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new ChooseFilePage(this.m_client));
        }

        public void LoadScan(uint scanType)
        {
            ScanRequest scanRequest = new ScanRequest { Path = this.m_path, ScanType = scanType, ScanAgain = false};
            this.NavigationService.Navigate(new ScanningPage(this.m_client, scanRequest));
        }

        private void StaticScanButton_Clicked(object sender, RoutedEventArgs e)
        {
            LoadScan((uint)ScanTypes.STATIC);
        }

        private void DynamicScanButton_Clicked(object sender, RoutedEventArgs e)
        {
            if (!(this.m_path.EndsWith(".bat") || this.m_path.EndsWith(".exe") 
                || this.m_path.EndsWith(".vbs") || this.m_path.EndsWith(".ps") || this.m_path.EndsWith(".ps1")))
            {
                // Invalid extension/format
                this.Warning.Visibility = Visibility.Visible;
                this.ErrorLable.Content = "This file cannot be scanned dynamically (only: .exe, .bat, .vbs, .ps, .ps1)";
            }
            else
            {
                LoadScan((uint)ScanTypes.DYNAMIC);
            }
        }

        private void FullScanButton_Clicked(object sender, RoutedEventArgs e)
        {
            this.Warning.Visibility = Visibility.Hidden;
            if (!(this.m_path.EndsWith(".bat") || this.m_path.EndsWith(".exe")
                || this.m_path.EndsWith(".vbs") || this.m_path.EndsWith(".ps") || this.m_path.EndsWith(".ps1")))
            {
                bool? Result = new MessageBoxCustom("Do you want to scan it statically?", "This file cannot be scanned dynamically", MessageButtons.YesNo).ShowDialog();
                if (Result.Value) // yes - run it statically
                {
                    LoadScan((uint)ScanTypes.STATIC);
                }
            }
            else
            {
                LoadScan((uint)ScanTypes.BOTH);
            }
        }

    }
}
