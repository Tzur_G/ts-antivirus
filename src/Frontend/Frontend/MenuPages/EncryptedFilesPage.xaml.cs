﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Frontend.TsClient;
using Frontend.TsClient.Requests;
using Frontend.TsClient.Responses;
using Newtonsoft.Json;

namespace Frontend.MenuPages
{
    /// <summary>
    /// Interaction logic for EncryptedFilesPage.xaml
    /// </summary>
    public partial class EncryptedFilesPage : Page
    {
        private Communicator m_client;
        List<PathItem> paths = new List<PathItem>();

        public class PathItem
        {
            public string Path { get; set; }
        }

        public EncryptedFilesPage(Communicator client)
        {
            InitializeComponent();
            this.m_client = client;
            LoadEncryptedFiles();
        }

        private void DecryptFile_Clicked(object sender, RoutedEventArgs e)
        {
            this.Warning.Visibility = Visibility.Hidden;
            bool success = false;
            string path = paths[this.EncryptedFilesList.SelectedIndex].Path;
            DecryptFileRequest request = new DecryptFileRequest { Path = path };
            GeneralResponse response = this.m_client
                .SendAPIRequest<DecryptFileRequest>((int)PacketTypes.DECRYPT_FILE, request);

            if (response.Op == (int)PacketTypes.DECRYPT_FILE)
            {
                DecryptFileResponse decryptFileResponse = JsonConvert.DeserializeObject<DecryptFileResponse>(response.Buffer);
                if (decryptFileResponse.Status == 1)
                {
                    success = true;
                    // Delete row from DataGrid
                    int index = this.EncryptedFilesList.SelectedIndex;
                    this.EncryptedFilesList.ItemsSource = null;
                    paths.RemoveAt(index);
                    this.EncryptedFilesList.ItemsSource = paths;
                    if (paths.Count == 0)
                    {
                        this.Empty.Visibility = Visibility.Visible;
                    }
                }
            }

            if (!success)
            {
                this.Warning.Visibility = Visibility.Visible;
                this.ErrorLable.Content = "Cannot Decrypt file";
            }
        }

        private void ShowMoreDetails_Clicked(object sender, RoutedEventArgs e)
        {
            this.Warning.Visibility = Visibility.Hidden;
            string path = paths[this.EncryptedFilesList.SelectedIndex].Path;
            GetFileInfoRequest request = new GetFileInfoRequest { Path = path };
            GeneralResponse response = this.m_client
                .SendAPIRequest<GetFileInfoRequest>((int)PacketTypes.GET_FILE_INFO, request);

            if (response.Op == (int)PacketTypes.GET_FILE_INFO)
            {
                GetFileInfoResponse getFileInfoResponse = JsonConvert.DeserializeObject<GetFileInfoResponse>(response.Buffer);
                // Open scan results window
                ShowScanResultsPage page = new ShowScanResultsPage(this.m_client, getFileInfoResponse.FileInfo);
                this.NavigationService.Content = page;
            }
            else
            {
                this.Warning.Visibility = Visibility.Visible;
                this.ErrorLable.Content = "Cannot show details of file";
            }
        }

        private void LoadEncryptedFiles()
        {
            GetFilesRequest request = new GetFilesRequest { OnlyEncrypted = true };
            GeneralResponse response = this.m_client
                .SendAPIRequest<GetFilesRequest>((int)PacketTypes.GET_FILES, request);

            if (response.Op == (int)PacketTypes.GET_FILES)
            {
                GetFilesResponse getFilesResponse = JsonConvert.DeserializeObject<GetFilesResponse>(response.Buffer);
                foreach (string path in getFilesResponse.Paths)
                {
                    paths.Add(new PathItem { Path = path });
                }
                this.EncryptedFilesList.ItemsSource = paths;
                if (paths.Count == 0)
                {
                    this.Empty.Visibility = Visibility.Visible;
                }
            }
            else
            {
                this.Warning.Visibility = Visibility.Visible;
                this.ErrorLable.Content = "Cannot show encrypted files";
            }
        }
    }
}
