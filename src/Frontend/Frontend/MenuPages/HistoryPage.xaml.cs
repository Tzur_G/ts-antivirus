﻿using Frontend.TsClient;
using Frontend.TsClient.Requests;
using Frontend.TsClient.Responses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Frontend.MenuPages
{
    /// <summary>
    /// Interaction logic for HistoryPage.xaml
    /// </summary>
    public partial class HistoryPage : Page
    {
        private Communicator m_client;
        List<PathItem> paths = new List<PathItem>();

        public class PathItem
        {
            public string Path { get; set; }
        }

        public HistoryPage(Communicator client)
        {
            InitializeComponent();
            this.m_client = client;
            LoadHistory();
        }

        private void ShowMoreDetails_Clicked(object sender, RoutedEventArgs e)
        {
            this.Warning.Visibility = Visibility.Hidden;
            string path = paths[this.HistoryList.SelectedIndex].Path;
            GetFileInfoRequest request = new GetFileInfoRequest { Path = path };
            GeneralResponse response = this.m_client
                .SendAPIRequest<GetFileInfoRequest>((int)PacketTypes.GET_FILE_INFO, request);

            if (response.Op == (int)PacketTypes.GET_FILE_INFO)
            {
                GetFileInfoResponse getFileInfoResponse = JsonConvert.DeserializeObject<GetFileInfoResponse>(response.Buffer);
                // Open scan results window
                ShowScanResultsPage page = new ShowScanResultsPage(this.m_client, getFileInfoResponse.FileInfo);
                this.NavigationService.Content = page;
            }
            else
            {
                this.Warning.Visibility = Visibility.Visible;
                this.ErrorLable.Content = "Cannot show details of file";
            }
        }

        private void LoadHistory()
        {
            GetFilesRequest request = new GetFilesRequest { OnlyEncrypted = false };
            GeneralResponse response = this.m_client
                .SendAPIRequest<GetFilesRequest>((int)PacketTypes.GET_FILES, request);

            if (response.Op == (int)PacketTypes.GET_FILES)
            {
                GetFilesResponse getFilesResponse = JsonConvert.DeserializeObject<GetFilesResponse>(response.Buffer);
                foreach (string path in getFilesResponse.Paths)
                {
                    paths.Add(new PathItem { Path = path });
                }
                this.HistoryList.ItemsSource = paths;
                if (paths.Count == 0)
                {
                    this.Empty.Visibility = Visibility.Visible;
                }
            }
            else
            {
                this.Warning.Visibility = Visibility.Visible;
                this.ErrorLable.Content = "Cannot show scanned files";
            }
        }
    }
}
