﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Frontend.TsClient;
using Microsoft.Win32;
using Frontend.TsClient.Requests;
using Frontend.TsClient.Responses;
using Newtonsoft.Json;
using System.Threading;

namespace Frontend.MenuPages
{
    /// <summary>
    /// Interaction logic for ScanningPage.xaml
    /// </summary>
    public partial class ScanningPage : Page
    {
        private Communicator m_client;
        private string path;
        ScanRequest m_scanRequest;

        CancellationTokenSource tokenSource;
        CancellationToken token;

        public ScanningPage(Communicator client, ScanRequest scanRequest)
        {
            InitializeComponent();

            // to cancel the task
            this.tokenSource = new CancellationTokenSource();
            this.token = tokenSource.Token;

            this.m_scanRequest = scanRequest;
            this.m_client = client;
            this.path = scanRequest.Path;
            this.FilePathTextBlock.Text = this.path;
            if (scanRequest.ScanType == (uint)ScanTypes.STATIC)
                this.ScanHeader.Content = "Static scan";
            else if (scanRequest.ScanType == (uint)ScanTypes.DYNAMIC)
                this.ScanHeader.Content = "Dynamic scan";
            else if (scanRequest.ScanType == (uint)ScanTypes.BOTH)
                this.ScanHeader.Content = "Full scan";

            var test = Communicator.ConstructPacket((int)PacketTypes.SCAN, JsonConvert.SerializeObject(scanRequest, Formatting.Indented));
            this.m_client.SendData(test);
            this.m_client.isScanning = true;
            StartScan();
        }

        public void LookForResponse()
        {
            if (this.m_client.Socket.GetStream().DataAvailable)
            {
                // Cancel the task
                this.tokenSource.Cancel();
                tokenSource.Dispose();

                GeneralResponse response = this.m_client.GetData();
                this.m_client.isScanning = false;

                if (response.Op == (int)PacketTypes.SCAN)
                {
                    // Show scan results
                    ScanResponse scanResponse = JsonConvert.DeserializeObject<ScanResponse>(response.Buffer);
                    ShowScanResultsPage page = new ShowScanResultsPage(this.m_client, this.m_scanRequest, scanResponse);
                    this.NavigationService.Content = page;
                }
                else // if this is an error response
                {
                    ErrorResponse err = JsonConvert.DeserializeObject<ErrorResponse>(response.Buffer);
                    string errorMsg = err.Message;
                    if (errorMsg.StartsWith("ERROR"))
                        errorMsg = errorMsg.Substring(7); // to remove the "ERROR: "

                    _ = new MessageBoxCustom(errorMsg, "Error", MessageButtons.Ok).ShowDialog();

                    // Go back
                    Menu currWindow = Application.Current.Windows.OfType<Menu>().SingleOrDefault(x => x.IsActive);
                    currWindow.Close();
                    Menu menu = new Menu(this.m_client);
                    menu.ShowDialog();
                }
            }
        }

        private async void StartScan()
        {
            await Task.Run(() =>
            {
                SetInterval(() => xf(this.token), TimeSpan.FromSeconds(2));
            }, this.token);
        }

        public async Task SetInterval(Action action, TimeSpan timeout)
        {
            await Task.Delay(timeout).ConfigureAwait(false);
            action();
            SetInterval(action, timeout);
        }

        internal void xf(CancellationToken token)
        {
            if (token.IsCancellationRequested) // Was cancellation already requested?
                token.ThrowIfCancellationRequested();

            this.Dispatcher.Invoke(() =>
            {
                LookForResponse();
            });
        }
    }
}
