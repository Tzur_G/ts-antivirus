﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Frontend.TsClient;
using Frontend.TsClient.Requests;
using Frontend.TsClient.Responses;
using Microsoft.Win32;
using Newtonsoft.Json;

namespace Frontend.MenuPages
{
    /// <summary>
    /// Interaction logic for ShowScanResultsPage.xaml
    /// </summary>
    public partial class ShowScanResultsPage : Page
    {
        private Communicator m_client;
        private string reportStr;
        private bool _isAfterScan;

        // (1) Constructor for ScanningPage
        public ShowScanResultsPage(Communicator client, ScanRequest scanRequest, ScanResponse scanResponse)
        {
            InitializeComponent();
            this._isAfterScan = true;
            this.m_client = client;
            this.reportStr = "";
            HandleResponse(scanRequest, scanResponse);
        }

        // (2) Constructor for button 'Details' from History/EncryptedFilesPage
        public ShowScanResultsPage(Communicator client, FileInformation fileInfo)
        {
            InitializeComponent();
            this._isAfterScan = false;
            this.m_client = client;
            this.reportStr = "";
            ShowFileInformation(fileInfo);
        }

        // (1) Show information on file
        public async void HandleResponse(ScanRequest scanRequest, ScanResponse scanResponse)
        {
            // Show results
            if (scanResponse.IsMalicious)
            {
                this.GridBorder.BorderBrush = Brushes.Red;
                this.FileTypeTextBlock.Background = Brushes.Red;
                this.FileTypeTextBlock.Text = "Malicious";
            }
            else
            {
                this.GridBorder.BorderBrush = Brushes.Green;
                this.FileTypeTextBlock.Background = Brushes.Green;
                this.FileTypeTextBlock.Text = "Clean";
            }

            reportStr = "Path: " + scanRequest.Path + "\n";

            try
            {
                Dictionary<string, string> reportDict = JsonConvert.DeserializeObject
                                                            <Dictionary<string, string>>(scanResponse.Report);
                if (reportDict.ContainsKey("s"))
                    reportStr += "\nStatic scan result:\n" + reportDict["s"] + "\n";
                if (reportDict.ContainsKey("d"))
                    reportStr += "\nDynamic scan result:\nScan Log:\n" + reportDict["d"] + "\n";
            }
            catch
            {
                _ = new MessageBoxCustom("An error occurred", "Error", MessageButtons.Ok).ShowDialog();
                XButton_Clicked(null, null);
                return;
            }

            this.ResultsTextBlock.Text = reportStr;
            reportStr = "Scan Results:\n" + reportStr;

            if (scanResponse.Status == (uint)StatusType.REPORT_ALREADY_EXIST)
            {
                await Task.Delay(TimeSpan.FromSeconds(2));
                bool? Result = new MessageBoxCustom("Would you like to scan again?", "The file has been scanned before", MessageButtons.YesNo).ShowDialog();
                if (Result.Value) // scan again
                {
                    scanRequest.ScanAgain = true;
                    // Go back
                    Menu currWindow = Application.Current.Windows.OfType<Menu>().SingleOrDefault(x => x.IsActive);
                    currWindow.PageInMenu.Content = new ScanningPage(this.m_client, scanRequest);
                }

            }
        }

        // (2) Show information on file
        public void ShowFileInformation(FileInformation fileInfo)
        {
            // Show results
            if (fileInfo.IsMalicious)
            {
                this.GridBorder.BorderBrush = Brushes.Red;
                this.FileTypeTextBlock.Background = Brushes.Red;
                this.FileTypeTextBlock.Text = "Malicious";
            }
            else
            {
                this.GridBorder.BorderBrush = Brushes.Green;
                this.FileTypeTextBlock.Background = Brushes.Green;
                this.FileTypeTextBlock.Text = "Clean";
            }

            this.DateTextBlock.Text = fileInfo.ScanDate + " " + fileInfo.ScanTime;
            reportStr = "Path: " + fileInfo.Path + "\n";
            reportStr += "Hash: " + fileInfo.Hash + "\n";

            try
            {
                Dictionary<string, string> reportDict = JsonConvert.DeserializeObject
                                                            <Dictionary<string, string>>(fileInfo.Report);
                if (reportDict.ContainsKey("s"))
                    reportStr += "\nStatic scan result:\n" + reportDict["s"] + "\n";
                if (reportDict.ContainsKey("d"))
                    reportStr += "\nDynamic scan result:\n" + reportDict["d"] + "\n";
            }
            catch
            {
                _ = new MessageBoxCustom("An error occurred", "Error", MessageButtons.Ok).ShowDialog();
                XButton_Clicked(null, null);
                return;
            }

            this.ResultsTextBlock.Text = reportStr;
            reportStr = "Scan Results:\nDate: " + this.DateTextBlock.Text + "\n" + reportStr;
        }

        private void SaveAsButton_Clicked(object sender, System.EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "(*.txt)|*.txt|All Files (*.*)|*.*";
            saveFileDialog1.Title = "Save File";
            saveFileDialog1.ShowDialog();

            // If the file name is not an empty string open it for saving.
            if (saveFileDialog1.FileName != "")
            {
                // Saves the file via a FileStream created by the OpenFile method.
                System.IO.FileStream fs = (System.IO.FileStream)saveFileDialog1.OpenFile();
                byte[] info = new UTF8Encoding(true).GetBytes(reportStr);
                fs.Write(info, 0, info.Length);
                fs.Close();
            }
        }

        // Get back to the state when the user need to choose a file to scan
        private void XButton_Clicked(object sender, RoutedEventArgs e)
        {
            if (this._isAfterScan || (sender == null && e == null))
            { // Go back to choose file to scan page
                Menu currWindow = Application.Current.Windows.OfType<Menu>().SingleOrDefault(x => x.IsActive);
                currWindow.Close();
                Menu menu = new Menu(this.m_client);
                menu.ShowDialog();
            }
            else 
            { // Go back
                this.NavigationService.GoBack();
            }
        }
    }
}
