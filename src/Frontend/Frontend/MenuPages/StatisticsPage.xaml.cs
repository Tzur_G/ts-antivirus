﻿using Frontend.TsClient;
using Frontend.TsClient.Requests;
using Frontend.TsClient.Responses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Frontend.MenuPages
{
    /// <summary>
    /// Interaction logic for StatisticsPage.xaml
    /// </summary>
    public partial class StatisticsPage : Page
    {
        private Communicator m_client;

        public class PiePoint
        {
            public string Name { get; set; }
            public Int16 Share { get; set; }
        }
        private ObservableCollection<PiePoint> _pieCollection;
        public ObservableCollection<PiePoint> PieCollection { get { return _pieCollection; } set { _pieCollection = value; } }

        public StatisticsPage(Communicator client)
        {
            InitializeComponent();
            this.m_client = client;
        }

        private void Chart_Loaded(object sender, RoutedEventArgs e)
        {
            // Send statistics request
            GetStatisticsRequest request = new GetStatisticsRequest { };
            GeneralResponse generalResponse = this.m_client.
                    SendAPIRequest<GetStatisticsRequest>((int)PacketTypes.GET_STATISTICS, request);

            if (generalResponse.Op == (int)PacketTypes.GET_STATISTICS)
            {
                GetStatisticsResponse statisticsResponse = JsonConvert.DeserializeObject<GetStatisticsResponse>(generalResponse.Buffer);

                this.TotalFilesOfUser.Text = "You scanned\n" + (statisticsResponse.NumOfUserCleanFiles + statisticsResponse.NumOfUserMaliciousFiles) + " files";
                this.TotalFiles.Text = "Our system scanned\n" + (statisticsResponse.NumOfCleanFiles + statisticsResponse.NumOfMaliciousFiles) + " files";
                this.MaliciousFiles.Text = statisticsResponse.NumOfMaliciousFiles + " of the files in our system are malicios";
                this.EncryptedFiles.Text = "We encrypted\n" + statisticsResponse.NumOfUserEncryptedFiles + " of your files";

                // load the pie
                PieCollection = new ObservableCollection<PiePoint>();
                PieCollection.Add(new PiePoint { Name = "Clean", Share = (short)statisticsResponse.NumOfUserCleanFiles });
                PieCollection.Add(new PiePoint { Name = "Malicious", Share = (short)statisticsResponse.NumOfUserMaliciousFiles });
                this.Pie.ItemsSource = PieCollection;
            }
            else // if this is an error response
            {
                ErrorResponse err = JsonConvert.DeserializeObject<ErrorResponse>(generalResponse.Buffer);
                _ = new MessageBoxCustom(err.Message, "Error", MessageButtons.Ok).ShowDialog();
            }
        }
    }
}
