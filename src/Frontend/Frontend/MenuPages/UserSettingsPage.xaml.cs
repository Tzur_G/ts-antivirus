﻿using Frontend.TsClient;
using Frontend.TsClient.Requests;
using Frontend.TsClient.Responses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Frontend.MenuPages
{
    /// <summary>
    /// Interaction logic for UserSettingsPage.xaml
    /// </summary>
    public partial class UserSettingsPage : Page
    {
        private Communicator m_client;

        public UserSettingsPage(Communicator client)
        {
            InitializeComponent();
            this.m_client = client;
        }

        // When pressing on the 'Update' button
        private void UpdateButton_Clicked(object sender, RoutedEventArgs e)
        {
            bool valid = false;
            this.Warning.Visibility = Visibility.Hidden;
            this.ErrorLable.Content = "Choose field and its new value";
            this.NewValTextBox.BorderBrush = Brushes.White;

            // Check validation
            switch (this.OptionsComboBox.SelectedIndex)
            {
                case 0: // User Name
                    if (this.NewValTextBox.Text == "")
                        this.ErrorLable.Content = "User name can not be empty";
                    else if (this.NewValTextBox.Text == this.m_client.Username)
                        this.ErrorLable.Content = "This is the value of your existing user name";
                    else if (!UserValidation.IsUsernameValid(this.NewValTextBox.Text))
                        this.ErrorLable.Content = "Invalid user name";
                    else
                        valid = true;
                    break;
                case 1: // Password
                    if (this.NewValTextBox.Text == "")
                        this.ErrorLable.Content = "Password can not be empty";
                    else if (this.NewValTextBox.Text == this.m_client.Password)
                        this.ErrorLable.Content = "This is the value of your existing password";
                    else if (!UserValidation.IsPasswordValid(this.NewValTextBox.Text))
                        this.ErrorLable.Content = "Invalid password";
                    else
                        valid = true;
                    break;
                case 2: // Email
                    if (this.NewValTextBox.Text == "")
                        this.ErrorLable.Content = "Email can not be empty";
                    else if (this.NewValTextBox.Text == this.m_client.Email)
                        this.ErrorLable.Content = "This is the value of your existing email";
                    else if (!UserValidation.IsMailValid(this.NewValTextBox.Text))
                        this.ErrorLable.Content = "Invalid email address";
                    else
                        valid = true;
                    break;
            }

            if (valid)
            {
                // Update
                SettingsRequest request = new SettingsRequest
                { InputType = (uint)this.OptionsComboBox.SelectedIndex, Input = this.NewValTextBox.Text };
                GeneralResponse generalResponse = this.m_client.
                    SendAPIRequest<SettingsRequest>((int)PacketTypes.EDIT_SETTINGS, request);

                if (generalResponse.Op == (int)PacketTypes.EDIT_SETTINGS) // status = 1
                {
                    SettingsResponse signupResponse = JsonConvert.DeserializeObject<SettingsResponse>(generalResponse.Buffer);
                    if (signupResponse.Status == 1)
                    {
                        string msg = "";
                        switch (this.OptionsComboBox.SelectedIndex)
                        {
                            case 0: // User Name
                                this.m_client.Username = this.NewValTextBox.Text;
                                Menu currWindow = Application.Current.Windows.OfType<Menu>().SingleOrDefault(x => x.IsActive);
                                currWindow.usernameLabel.Content = this.NewValTextBox.Text;
                                msg = "User name updated";
                                break;
                            case 1: // Password
                                this.m_client.Password = this.NewValTextBox.Text;
                                msg = "Password updated";
                                break;
                            case 2: // Email
                                this.m_client.Email = this.NewValTextBox.Text;
                                msg = "Email updated";
                                break;
                        }
                        _ = new MessageBoxCustom(msg, "Success", MessageButtons.Ok).ShowDialog();
                    }
                }
                else // if this is an error response
                {
                    ErrorResponse err = JsonConvert.DeserializeObject<ErrorResponse>(generalResponse.Buffer);
                    this.Warning.Visibility = Visibility.Visible;
                    this.ErrorLable.Content = err.Message;
                }
            }
            else
            {
                this.Warning.Visibility = Visibility.Visible;
                this.NewValTextBox.BorderBrush = Brushes.Red;
            }
        }


        private void OptionsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.NewValTextBox.BorderBrush = Brushes.White;
            this.NewValTextBox.Text = "";
            this.SyntaxGrid.Visibility = Visibility.Visible;
            switch (this.OptionsComboBox.SelectedIndex)
            {
                case 0: // User Name
                    this.SyntaxGrid.Height = 50;
                    this.FieldSyntax.Text = "Use between 4 to 20 charcters\nstart with a letter";
                    break;
                case 1: // Password
                    this.SyntaxGrid.Height = 100;
                    this.FieldSyntax.Text = "Use between 6 to 20 charcters\nat least one number\nat least one uppercase letter\nat least one lowercase letter";
                    break;
                default:
                    this.SyntaxGrid.Visibility = Visibility.Hidden;
                    break;
            }
        }
    }
}
