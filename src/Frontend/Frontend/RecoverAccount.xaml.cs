﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Frontend.TsClient;
using Frontend.TsClient.Requests;
using Frontend.TsClient.Responses;
using Newtonsoft.Json;

namespace Frontend
{
    /// <summary>
    /// Interaction logic for RecoverAccount.xaml
    /// </summary>
    public partial class RecoverAccount : Window
    {
        private Communicator m_client;

        public RecoverAccount(Communicator client)
        {
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen; // put this window in the middle of screen
            InitializeComponent();
            this.m_client = client;
            this.usernameBox.Focus();
        }

        // Go to login window
        private void BackToLoginButton_Clicked(object sender, RoutedEventArgs e)
        {
            this.Close();
            Login login = new Login(this.m_client);
            login.ShowDialog();
        }

        private void SendMail_Clicked(object sender, RoutedEventArgs e)
        {
            bool isValid = true;
            this.usernameBox.BorderBrush = (this.usernameBox.Text.Length == 0) ? Brushes.Red : Brushes.White;
            this.emailBox.BorderBrush = (this.emailBox.Text.Length == 0) ? Brushes.Red : Brushes.White;

            // Check validation
            if ((this.usernameBox.Text.Length == 0) || (this.emailBox.Text.Length == 0))
            {
                isValid = false;
                this.Warning.Visibility = Visibility.Visible;
                this.ErrorLable.Content = "Fill all Fields";
            }
            else
            {
                if (!UserValidation.IsUsernameValid(this.usernameBox.Text))
                {
                    this.usernameBox.BorderBrush = Brushes.Red;
                    isValid = false;
                }
                if (!UserValidation.IsMailValid(this.emailBox.Text))
                {
                    this.emailBox.BorderBrush = Brushes.Red;
                    isValid = false;
                }

                if (!isValid)
                {
                    this.Warning.Visibility = Visibility.Visible;
                    this.ErrorLable.Content = "Invalid Fields";
                }
            }

            if (isValid)
            {
                SignupRequest signupRequest = new SignupRequest { Username = this.usernameBox.Text, Password = "", Email = this.emailBox.Text, Reset = true };
                GeneralResponse generalResponse = this.m_client.
                    SendAPIRequest<SignupRequest>((int)PacketTypes.SIGNUP, signupRequest);

                if (generalResponse.Op == (int)PacketTypes.SIGNUP) // status = 1
                {
                    SignupResponse signupResponse = JsonConvert.DeserializeObject<SignupResponse>(generalResponse.Buffer);
                    if (signupResponse.Status == 1)
                    {
                        bool? Result = new MessageBoxCustom("Would you like to continue?", "The email will be sent", MessageButtons.YesNo).ShowDialog();
                        if (Result.Value) // continue
                        {
                            this.m_client.Username = this.usernameBox.Text;
                            this.Close();
                            VerifyEmail verify = new VerifyEmail(this.m_client, this.emailBox.Text);
                            verify.ShowDialog();
                        }
                    }
                    else 
                    {
                        this.Warning.Visibility = Visibility.Visible;
                        this.ErrorLable.Content = "Username or email are not match";
                    }
                }
                else // if this is an error response
                {
                    ErrorResponse err = JsonConvert.DeserializeObject<ErrorResponse>(generalResponse.Buffer);
                    this.Warning.Visibility = Visibility.Visible;
                    this.ErrorLable.Content = err.Message;
                }
            }
        }

        // Go to signup window
        private void SignupButton_Clicked(object sender, MouseButtonEventArgs e)
        {
            this.Close();
            // Open the signup window
            SignUp signup = new SignUp(this.m_client);
            signup.ShowDialog();
        }
    }
}
