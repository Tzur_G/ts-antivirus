﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Frontend.TsClient;
using Frontend.TsClient.Requests;
using Frontend.TsClient.Responses;
using Newtonsoft.Json;

namespace Frontend
{
    /// <summary>
    /// Interaction logic for SignUp.xaml
    /// </summary>
    public partial class SignUp : Window
    {
        private Communicator m_client;

        public SignUp(Communicator client)
        {
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen; // put this window in the middle of screen
            InitializeComponent();
            this.m_client = client;
            this.usernameBox.Focus();
        }

        private void UsernameTextBox_Validation(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (!UserValidation.IsUsernameValid(this.usernameBox.Text))
                this.usernameBox.BorderBrush = Brushes.Red; // show warnning
            else
                this.usernameBox.BorderBrush = Brushes.White; // remove warnning
        }

        private void PasswordTextBox_Validation(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (!UserValidation.IsPasswordValid(this.passBox.Text))
                this.passBox.BorderBrush = Brushes.Red; // show warnning
            else
                this.passBox.BorderBrush = Brushes.White; // remove warnning
        }

        private void EmailTextBox_Validation(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (!UserValidation.IsMailValid(this.emailBox.Text))
                this.emailBox.BorderBrush = Brushes.Red; // show warnning
            else
                this.emailBox.BorderBrush = Brushes.White; // remove warnning
        }

        private void SignupButton_Clicked(object sender, RoutedEventArgs e)
        {
            if (!UserValidation.AreNamePassMailFulled(this.usernameBox, this.passBox, this.emailBox)) 
                // check if there ate no empty fields
            {
                this.Warning.Visibility = Visibility.Visible;
                this.ErrorLable.Content = "Fill all Fields";
            }
            else if (!UserValidation.AreNamePassMailValid(this.usernameBox, this.passBox, this.emailBox)) 
                // check if all fields are valid
            {
                this.Warning.Visibility = Visibility.Visible;
                this.ErrorLable.Content = "Invalid Fields";
            }
            else
            {
                // Send request
                SignupRequest signupRequest = new SignupRequest { Username = this.usernameBox.Text, Password = this.passBox.Text, Email = this.emailBox.Text, Reset = false};
                GeneralResponse generalResponse = this.m_client.
                    SendAPIRequest<SignupRequest>((int)PacketTypes.SIGNUP, signupRequest);

                if (generalResponse.Op == (int)PacketTypes.SIGNUP) // status = 1
                {
                    SignupResponse signupResponse = JsonConvert.DeserializeObject<SignupResponse>(generalResponse.Buffer);
                    if (signupResponse.Status == 1)
                    {
                        this.m_client.Username = this.usernameBox.Text;
                        this.m_client.Password = this.passBox.Text;
                        this.m_client.Email = this.emailBox.Text;

                        // Open menu
                        this.Close();
                        Menu menuWindow = new Menu(this.m_client);
                        menuWindow.ShowDialog();
                    }
                }
                else // if this is an error response
                {
                    ErrorResponse err = JsonConvert.DeserializeObject<ErrorResponse>(generalResponse.Buffer);
                    this.Warning.Visibility = Visibility.Visible;
                    this.ErrorLable.Content = err.Message;
                }
            }
        }

        // Go to login window
        private void LoginButton_Clicked(object sender, MouseButtonEventArgs e)
        {
            this.Close();
            // Open the login window
            Login login = new Login(this.m_client);
            login.ShowDialog();          
        }
    }
}
