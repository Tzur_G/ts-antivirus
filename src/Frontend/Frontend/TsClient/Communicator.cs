﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Windows;
using Frontend.TsClient;
using Newtonsoft.Json;
using Frontend.TsClient.Responses;
using Frontend.TsClient.Requests;

namespace Frontend.TsClient
{
    public class Communicator
    {
        public const int OpSize = 1;
        public const int LengthSize = 4;

        public TcpClient Socket;
        public string Username;
        public string Password;
        public string Email;
        public bool isScanning;

        public Communicator()
        {
            this.Socket = new TcpClient();
            this.Username = "";
            this.Password = "";
            this.Email = "";
            this.isScanning = false;
        }

        public void NoConnection()
        {
            this.Disconnect();
            _ = new MessageBoxCustom("Lost connection with the server", "Connection Failed", MessageButtons.Ok).ShowDialog();
            Application.Current.Shutdown(); // close the app
            Environment.Exit(1);
        }

        public void Connect()
        {
            try
            { 
                this.Socket.Connect("127.0.0.1", 14014);
                NetworkStream serverStream = this.Socket.GetStream();
            }
            catch
            {
                NoConnection();
            }
        }

        // Check if user want to stop the scanning progress
        public bool QuitScan()
        {
            if (!this.isScanning)
            {
                return true;
            }

            _ = new MessageBoxCustom("Wait untill the scan is over", "Scan is in progress", MessageButtons.Ok).ShowDialog();
            return false;
        }

        public GeneralResponse GetData()
        {
            NetworkStream serverStream = this.Socket.GetStream();
            GeneralResponse generalResponse = new GeneralResponse { };
            byte[] opCode = new byte[OpSize];
            byte[] len = new byte[LengthSize];
            byte[] message;

            try
            {
                // Get the type of the message
                serverStream.Read(opCode, 0, OpSize);
                generalResponse.Op = opCode[0];

                // Get the length
                serverStream.Read(len, 0, LengthSize);
                int msgLen = BitConverter.ToInt32(len, 0);

                // Get the message
                message = new byte[msgLen];
                serverStream.Read(message, 0, msgLen);
                generalResponse.Buffer = Encoding.UTF8.GetString(message);
                return generalResponse;
            }
            catch
            {
                return new GeneralResponse { Op = (int)PacketTypes.ERROR, Buffer = "Error"};
            }
        }

        public void SendData(string data)
        {
            try
            {
                byte[] buffer = new ASCIIEncoding().GetBytes(data);
                this.Socket.GetStream().Write(buffer, 0, buffer.Length);
                this.Socket.GetStream().Flush();
            }
            catch
            {
                NoConnection();
            }
        }

        public static string ConstructPacket(int op, string json)
        {
            json = json.Replace("\r\n ", "");
            json = json.Replace("\r\n", "");
            string requestLength = (json.Length).ToString();
            for (int i = requestLength.Length; i < 4; i++)
            {
                requestLength = "0" + requestLength;
            }
            string result = op.ToString() + requestLength + json;
            return result;
        }

        public GeneralResponse SendAPIRequest<IRequest>(int op, IRequest request)
        {
            try
            {
                var test = ConstructPacket(op, JsonConvert.SerializeObject(request, Formatting.Indented));
                SendData(test);
            }
            catch
            {
                _ = new MessageBoxCustom("Lost connection with the server", "Connection Lost", MessageButtons.Ok).ShowDialog();
                Application.Current.Shutdown(); // close the app
                Environment.Exit(1);
            }

            GeneralResponse generalResponse = GetData();
            return generalResponse;
        }

        public void Disconnect()
        {
            this.Socket.Close();
        }
    }
}
