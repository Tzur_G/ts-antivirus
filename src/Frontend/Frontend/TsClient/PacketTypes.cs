﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frontend.TsClient
{
	public enum PacketTypes : uint
	{
		ERROR = 0,
		LOGIN = 1,
		SIGNUP = 2,
		LOGOUT = 3,
		SCAN = 4,
		EDIT_SETTINGS = 5,
		GET_FILES = 6,
		GET_FILE_INFO = 7,
		DECRYPT_FILE = 8,	
		GET_STATISTICS = 9	
	};

	public enum ScanTypes : uint 
	{ 
		STATIC = 0,
		DYNAMIC = 1,
		BOTH = 2
	}
}
