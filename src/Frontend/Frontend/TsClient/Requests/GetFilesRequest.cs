﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Frontend.TsClient.Requests
{
    class GetFilesRequest : IRequest
    {
        [JsonProperty("onlyEncrypted")]
        public bool OnlyEncrypted { get; set; }
    }
}
