﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Frontend.TsClient.Requests
{
    public class ScanRequest : IRequest
    {
        [JsonProperty("scanType")]
        public uint ScanType { get; set; }

        [JsonProperty("path")]
        public string Path { get; set; }

        [JsonProperty("scanAgain")]
        public bool ScanAgain { get; set; }
    }
}
