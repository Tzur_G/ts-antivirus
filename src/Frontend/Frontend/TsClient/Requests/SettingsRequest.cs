﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Frontend.TsClient.Requests
{
    class SettingsRequest : IRequest
    {
        [JsonProperty("inputType")]
        public uint InputType { get; set; }

        [JsonProperty("input")]
        public string Input { get; set; }
    }
}
