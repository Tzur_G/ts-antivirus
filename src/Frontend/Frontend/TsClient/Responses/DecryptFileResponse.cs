﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Frontend.TsClient.Responses
{
    class DecryptFileResponse : IResponse
    {
        [JsonProperty("status")]
        public uint Status { get; set; }
    }
}
