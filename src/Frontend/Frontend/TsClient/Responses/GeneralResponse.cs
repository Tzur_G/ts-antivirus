﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Frontend.TsClient.Responses
{
    public class GeneralResponse
    {
        public uint Op { get; set; }
        public string Buffer { get; set; }
    }
}
