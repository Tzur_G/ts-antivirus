﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Frontend.TsClient.Responses
{
    public class FileInformation
    {
        [JsonProperty("path")]
        public string Path { get; set; }

        [JsonProperty("hash")]
        public string Hash { get; set; }

        [JsonProperty("report")]
        public string Report { get; set; }

        [JsonProperty("scanDate")]
        public string ScanDate { get; set; }

        [JsonProperty("scanTime")]
        public string ScanTime { get; set; }

        [JsonProperty("isMalicious")]
        public bool IsMalicious { get; set; }

        [JsonProperty("isEncrypted")]
        public bool IsEncrypted { get; set; }      
    }

    class GetFileInfoResponse
    {
        [JsonProperty("fileInfo")]
        public FileInformation FileInfo { get; set; }
    }
}
