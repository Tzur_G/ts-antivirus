﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Frontend.TsClient.Responses
{
    class GetStatisticsResponse : IResponse
    {
        [JsonProperty("numOfCleanFiles")]
        public uint NumOfCleanFiles { get; set; }

        [JsonProperty("numOfMaliciousFiles")]
        public uint NumOfMaliciousFiles { get; set; }

        [JsonProperty("numOfUserCleanFiles")]
        public uint NumOfUserCleanFiles { get; set; }

        [JsonProperty("numOfUserMaliciousFiles")]
        public uint NumOfUserMaliciousFiles { get; set; }

        [JsonProperty("numOfUserEncryptedFiles")]
        public uint NumOfUserEncryptedFiles { get; set; }
    }
}
