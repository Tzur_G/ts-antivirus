﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Frontend.TsClient.Responses
{
    public class ScanResponse : IResponse
    {
        [JsonProperty("status")]
        public uint Status { get; set; }

        [JsonProperty("isMalicious")]
        public bool IsMalicious { get; set; }      

        [JsonProperty("report")]
        public string Report { get; set; }
    }

    enum StatusType : uint 
    {
        ERROR = 0,
        SUCCESS = 1,
        REPORT_ALREADY_EXIST = 2
    }
}
