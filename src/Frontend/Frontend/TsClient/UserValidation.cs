﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace Frontend.TsClient
{
    class UserValidation
    {
        public static bool IsUsernameValid(string username)
        {
            // Rules:
            // - Between 4 to 20 characters
            // - Starts with a letter
            return Regex.IsMatch(username, @"^[A-Za-z][A-Za-z0-9-\-\'.!@\$%\^()_+]{3,19}\z", RegexOptions.IgnoreCase);
        }

        public static bool IsPasswordValid(string password)
        {
            // Rules:
            // - Between 6 to 20 characters
            // - Need to contain at least 1 capital letter, 1 small letter, and 1 digit.
            bool upperCase = false, lowerCase = false, numeric = false; 
            if (password.Length < 6 || password.Length > 20) //define the lengths             
                return false;             
            
            for (int i = 0; i < password.Length; i++)             
            {                 
                if(password[i] >= 'A' && password[i] <= 'Z')
                {                     
                    upperCase = true;
                }                 
                if (password[i] >= 'a' &&  password[i] <= 'z')                 
                {
                    lowerCase = true;                 
                }                 
                if (password[i] >= '0' && password[i] <= '9')
                {                    
                    numeric = true;                 
                }             
            }             
            return upperCase && lowerCase && numeric;
        }

        public static bool IsMailValid(string emailaddress)
        {
            return Regex.IsMatch(emailaddress, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
        }

        // Checks if the username and the password are not empty 
        public static bool AreNamePassFulled(TextBox usernameBox, TextBox passTextBox, PasswordBox passBox = null)
        {
            bool isValid = true;
            usernameBox.BorderBrush = Brushes.White;
            passTextBox.BorderBrush = Brushes.White;

            if (usernameBox.Text.Length == 0)
            {
                usernameBox.BorderBrush = Brushes.Red; // show warnning
                isValid = false;
            }
            if (passTextBox.Text.Length == 0)
            {
                passTextBox.BorderBrush = Brushes.Red; // show warnning
                if (passBox != null)
                    passBox.BorderBrush = Brushes.Red; // show warnning
                isValid = false;
            }

            return isValid;
        }

        // Checks if the username, password and mail are not empty 
        public static bool AreNamePassMailFulled(TextBox usernameBox, TextBox passBox, TextBox emailBox)
        {
            bool isValid = true;
            emailBox.BorderBrush = Brushes.White;

            if (!AreNamePassFulled(usernameBox, passBox))
            {
                isValid = false;
            }
            if (emailBox.Text.Length == 0)
            {
                emailBox.BorderBrush = Brushes.Red; // show warnning
                isValid = false;
            }

            return isValid;
        }

        // Checks if the username, password and mail are valid 
        public static bool AreNamePassMailValid(TextBox usernameBox, TextBox passBox, TextBox emailBox)
        {
            bool isValid = true;
            usernameBox.BorderBrush = Brushes.White;
            passBox.BorderBrush = Brushes.White;
            emailBox.BorderBrush = Brushes.White;

            if (!UserValidation.IsUsernameValid(usernameBox.Text))
            {
                usernameBox.BorderBrush = Brushes.Red; // show warnning
                isValid = false;
            }
            if (!UserValidation.IsPasswordValid(passBox.Text))
            {
                passBox.BorderBrush = Brushes.Red; // show warnning
                isValid = false;
            }
            if (!UserValidation.IsMailValid(emailBox.Text))
            {
                emailBox.BorderBrush = Brushes.Red; // show warnning
                isValid = false;
            }

            return isValid;
        }
    }
}
