﻿using Frontend.TsClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Frontend
{
    /// <summary>
    /// Interaction logic for VerifyEmail.xaml
    /// </summary>
    public partial class VerifyEmail : Window
    {
        private const uint RandKeyLength = 6;
        private const uint SecondsToWaitForCode = 120; // 2 minutes
        private const string SenderName = "TS-ANTIVIRUS";
        private const string SenderMail = "tsantivirus32@gmail.com";
        private const string SenderPassword = "tsantivirus2021";      

        private Communicator m_client;
        private string userMail;
        private uint randCode;

        private DispatcherTimer Timer;
        private uint timeLeft;

        public VerifyEmail(Communicator client, string email)
        {
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen; // put this window in the middle of screen
            InitializeComponent();
            this.m_client = client;
            this.userMail = email;
            this.randCode = GetRandomCode();
          
            Task.Factory.StartNew(async () =>
            {
                await Task.Delay(2000); // start process after 2 seconds

                // it only works in WPF
                Application.Current.Dispatcher.Invoke(() =>
                {
                    // initializate timer
                    this.Timer = new DispatcherTimer();
                    this.Timer.Interval = new TimeSpan(0, 0, 1);
                    this.Timer.Tick += Timer_Tick;

                    if (SendVerifyCode(this.userMail, this.randCode))
                    {
                        this.StatusLabel.Content = "The email was send.";
                        this.TimeBox.Content = "You have 02:00 minutes to enter the code";
                        this.CodeBox.Visibility = Visibility.Visible;
                        this.CodeBox.Focus();
                        this.timeLeft = SecondsToWaitForCode;
                        this.Timer.Start();
                    }
                    else
                    {
                        _ = new MessageBoxCustom("Cannot send email", "Error", MessageButtons.Ok).ShowDialog();
                        Back_Clicked(null, null);
                    }
                });
            });
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (this.timeLeft > 0)
            {
                this.timeLeft--;
                string timeStr = (timeLeft / 60).ToString("00") + ":" + (timeLeft % 60).ToString("00"); // show time in format of HH:mm
                this.TimeBox.Content = string.Format("You have {0} minutes to enter the code", timeStr);
            }
            else // time is up
            {
                this.Timer.Stop();
                this.WaitForVerify.Visibility = Visibility.Hidden;
                this.TimeoutGrid.Visibility = Visibility.Visible;
            }
        }

        public static uint GetRandomCode()
        {
            Random rnd = new Random();
            return Convert.ToUInt32(rnd.Next(Convert.ToInt32(Math.Pow(10, RandKeyLength - 1)), 
                Convert.ToInt32(Math.Pow(10, RandKeyLength))));
        }

        public static bool SendVerifyCode(string userMail, uint randCode)
        {
            var smtpClient = new SmtpClient("smtp.gmail.com")
            {
                Port = 587,
                Credentials = new NetworkCredential(SenderMail, SenderPassword),
                EnableSsl = true,
            };

            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(SenderMail, SenderName);
            mailMessage.Subject = "Verify Email Address";
            string content = "<p>Your Activation Code is: " + randCode.ToString() + "<p>";
            mailMessage.Body = content;
            mailMessage.IsBodyHtml = true;

            try
            {
                mailMessage.To.Add(userMail); // send to the user
                smtpClient.Send(mailMessage); // sending the message
                smtpClient.Dispose(); // end connection to smtp server
                return true;
            }
            catch // faliure
            {
                smtpClient.Dispose(); // end connection to smtp server
                return false;
            }
        }

        // Remove ability to copy, paste and cut
        private void TextBox_PreviewExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            if (e.Command == ApplicationCommands.Copy ||
                e.Command == ApplicationCommands.Cut ||
                e.Command == ApplicationCommands.Paste)
            {
                e.Handled = true;
            }
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void OnKeyUpHandler(object sender, KeyEventArgs e)
        {
            this.CodeBox.BorderBrush = Brushes.White;
            if (this.CodeBox.Text.Length == RandKeyLength)
            {
                if (Convert.ToUInt32(this.CodeBox.Text) == this.randCode)
                {
                    this.Close();
                    LoginAfterVerify login = new LoginAfterVerify(this.m_client, this.userMail);
                    login.ShowDialog();
                }
                else // wrong key
                {
                    this.CodeBox.BorderBrush = Brushes.Red;
                }
            }
        }

        private void SendAgain_Clicked(object sender, RoutedEventArgs e)
        {
            this.Close();
            VerifyEmail verify = new VerifyEmail(this.m_client, this.userMail);
            verify.ShowDialog();
        }

        // Go to Recover Account window
        private void Back_Clicked(object sender, RoutedEventArgs e)
        {
            this.Close();
            RecoverAccount recover = new RecoverAccount(this.m_client);
            recover.ShowDialog();
        }
    }
}
