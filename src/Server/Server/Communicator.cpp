#include "Communicator.h"

/*Constructor
input: none
output: nothing
*/
Communicator::Communicator(RequestHandlerFactory& handleFactory) : m_handleFactory(handleFactory)
{
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

/*Destructor
input: none
output: nothing
*/
Communicator::~Communicator()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

/*server socket binding and listening
input: none
output: none
*/
void Communicator::bindAndListen()
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(PORT); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*) & sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << PORT << std::endl;

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		handleNewClient();
	}
}

/*Function gets new client and creates thread for each one of them
input: none
output: none
*/
void Communicator::handleNewClient()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept
	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;

	m_clients[client_socket] = new LoginRequestHandler(m_handleFactory.createLoginRequestHandler());

	// the function that handle the conversation with the client
	std::thread t1(&Communicator::startHandleRequests, this, client_socket);
	t1.detach();

}

/*Function takes care of the user
input: client socket
output: none
*/
void Communicator::startHandleRequests(SOCKET clientSocket)
{
	try
	{
		while (true)
		{
			std::string buffer;
			//get the message
			char* m = new char[1];
			int res = recv(clientSocket, m, 1, 0);
			if (!res || res == INVALID_SOCKET)
			{
				throw std::exception();
			}
			buffer += m[0];
			delete[] m;
			char* data_len = new char[4];
			res = recv(clientSocket, data_len, 4, 0);
			if (!res || res == INVALID_SOCKET)
			{
				throw std::exception();
			}
			for (int i = 0; i < 4; i++)
			{
				buffer += data_len[i];
			}
			int len = 0;
			data_len[0] = '0';
			std::string s(data_len);
			len = stoi(s);
			delete[] data_len;
			if (len > 0)
			{
				char* message = new char[len];
				res = recv(clientSocket, message, len, 0);
				if (!res || res == INVALID_SOCKET)
				{
					throw std::exception();
				}
				for (int i = 0; i < len; i++)
				{
					buffer += message[i];
				}
				delete[] message;
			}
			//process it
			RequestInfo request;
			request.id = buffer[0] - 48;
			if (buffer[1] != '0')
			{
				request.id = request.id * 10 + buffer[1] - 48;
			}
			std::vector<std::uint8_t> buff(buffer.begin(), buffer.end());
			request.buffer = buff;
			time_t rec = time(NULL);
			request.receivalTime = rec;
			//send response
			if (m_clients[clientSocket]->isRequestRelevant(request))
			{
				RequestResult rt(m_clients[clientSocket]->handleRequest(request));
				if (rt.newHandler != nullptr)
				{
					m_clients[clientSocket] = rt.newHandler;
				}
				if (!rt.response.empty())
				{
					std::string message(rt.response.begin(), rt.response.end());
					send(clientSocket, message.c_str(), message.size(), 0);
				}
			}
			else
			{
				ErrorResponse er;
				er.message = "not relevant request";
				std::vector<std::uint8_t> message = JsonResponsePacketSerializer::serializeResponse(er);
				std::string result(message.begin(), message.end());
				send(clientSocket, result.c_str(), result.size(), 0);
			}
		}
	}
	catch (const std::exception & e)
	{
		m_clients[clientSocket]->logout();
		m_clients.erase(clientSocket);
		try
		{
			closesocket(clientSocket);
		}
		catch (...)
		{

		}
	}
}