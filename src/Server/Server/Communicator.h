#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <iostream>
#include<map>
#include <thread>
#include "IRequestHandler.h"
#include "LoginRequestHandler.h"
#include "ErrorResponse.h"
#include "JsonResponsePacketSerializer.h"

#define PORT 14014

class Communicator
{
private:
	std::map<SOCKET, IRequestHandler*> m_clients;
	RequestHandlerFactory& m_handleFactory;
	SOCKET _serverSocket;
	void handleNewClient();
public:
	Communicator(RequestHandlerFactory& handleFactory);
	~Communicator();
	void bindAndListen();
	void startHandleRequests(SOCKET clientSocket);
};
