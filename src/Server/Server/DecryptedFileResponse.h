#pragma once
#include <iostream>
#include "json.hpp"

using nlohmann::json;

struct DecryptedFileResponse
{
	unsigned int status;
};

inline void to_json(json& j, const DecryptedFileResponse& res)
{
	j = json{ {"status", res.status} };
}

inline void from_json(const json& j, DecryptedFileResponse& res)
{
	j.at("status").get_to(res.status);
}