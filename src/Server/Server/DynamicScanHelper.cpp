#include "DynamicScanHelper.h"


std::map<std::string, std::string> DynamicScanHelper::events = { {"01", "Event Viewer- file history backup log: "},
												   {"2", "Event Viewer- backup log: "},
												   {"03", "CVE - 2020 - 16939"},
												   {"4", "NtCreateFile"},
												   {"05", "CVE- 2020 - 16959"},
												   {"6", "CreateProcess"},
												   {"7", "The process tried to raise its privileges"},
												   {"8", "CreateRemoteThread"},
												   {"9", "CreateThread"},
												   {"010", "IsDebuggerPresent/CheckRemoteDebuggerPresent"},
												   {"11", "OpenFile"},
												   {"12", "OpenProcess"},
												   {"13", "ReadProcessMemory"},
												   {"14", "RegCreateKey"},
												   {"15", "RegOpenKey"},
												   {"016", "Task scheduler- CoCreateInstance"},
												   {"17", "URLDownloadToFile"},
												   {"18", "VirtualProtect"},
												   {"19", "WriteProcessMemory"},
												   {"020", "Melt File"},
												   {"21", "Install programs in path: "},
												   {"22", "Documents change in path: "},
												   {"022", "Backup folder change in path: "},
												   {"023", "Security program disabled"},
												   {"24", "Privilege escalation- "},
												   {"024", "Privileges escalation"},
												   {"25", "Access to forbidden key in registry"},
												   {"025", "The forbidden key is: "},
												   {"026", "Automatically run using start up folder that in path: "},
												   {"27", "Download file/string from the internet"},
};

std::map<std::string, unsigned int> DynamicScanHelper::eventsCounter = {
												   {"2", 0},
												   {"4", 0},
												   {"6", 0},
												   {"7", 0},
												   {"8", 0},
												   {"9", 0},
												   {"11", 0},
												   {"12", 0},
												   {"13", 0},
												   {"14", 0},
												   {"15", 0},
												   {"17", 0},
												   {"18", 0},
												   {"19", 0},
												   {"21", 0},
												   {"22", 0},
												   {"24", 0},
												   {"25", 0},
												   {"27", 0},
};

/*Function is running dynamic scan
input: file for the scanning and db
ouput: pair that contains false and error occurred or file is safe or true and file is malicious
*/
std::pair<bool, std::string> DynamicScanHelper::runDynamicScan(File file, IDatabase* db, bool scanAgain)
{
	if (!scanAgain && db->doesHashExist(file.getFileHash())) //if hash exists in db then send scanning report to the user, otherwise continue the scan
	{
		throw db->getReport(file.getFileHash());
	}

	std::string InjectorPath = DIRECTORY + std::string("\\Injector.exe");

	VMHelper* vmObj = new VMHelper((char*)VMX_PATH);
	if (vmObj->powerOn())
	{
		vmObj->deleteDirectoryFromVm(DIRECTORY); // if somthing was left from the last scan

		// create a folder
		if (!vmObj->runScript("mkdir c:\\vmTest"))
		{
			throw std::pair<bool, std::string>(false, "ERROR: The machine was shut down before the scan");
		}

		// copy all the necessary files for the scan
		if (!vmObj->copyFileFromPCToVM(DIRECTORY, DIRECTORY))
		{
			throw std::pair<bool, std::string>(false, "ERROR: The machine was shut down before the scan");
		}

		// copy the suspicious file to the vm
		std::string fileName = Utilities::findDstName(file.getFileName(), DIRECTORY);
		if (!vmObj->copyFileFromPCToVM(file.getFilePath().c_str(), fileName.c_str()))
		{
			throw std::pair<bool, std::string>(false, "ERROR: The machine was shut down before the scan");
		}

		// run the injector
		if (!vmObj->runInjector(InjectorPath.c_str(), fileName.c_str()))
		{
			throw std::pair<bool, std::string>(false, "ERROR: The scan was stopped in the middle");
		}

		// get the results
		if (!vmObj->copyFileFromVMToPC(LOG_PATH, LOG_PATH))
		{
			throw std::pair<bool, std::string>(false, "ERROR: The scan was stopped in the middle");
		}

		vmObj->deleteDirectoryFromVm(DIRECTORY); // delete the folder			
		vmObj->powerOff(); // turn off the vm
	}
	else
	{
		throw std::pair<bool, std::string>(false, "ERROR: can't open the vmware");
	}

	delete (vmObj);
	return getReport();
}

/*Function parses the log
input: none
output: pair that contains false and error occurred or file is safe or true and file is malicious
*/
std::pair<bool, std::string> DynamicScanHelper::getReport()
{
	std::ifstream infile(LOG_PATH);
	std::string line;
	std::vector<std::string> lines;
	while (std::getline(infile, line))
	{
		lines.push_back(line);
	}
	infile.close();
	for (int i = 0; i < lines.size(); i++)
	{
		if (lines[i][0] == '2')
		{
			lines.erase(lines.begin() + i);
			break;
		}
	}
	if (remove(LOG_PATH))
	{
		throw std::make_pair(false, "Error: can't delete the log file.");
	}
	std::string eventId = "";
	std::pair<bool, std::string> scanResult;
	int curr = 0;
	for (int i = 0; i < lines.size(); i++)
	{
		if (lines[i].length() >= MIN_LENGTH && (lines[i].at(2) == '0' || lines[i].at(2) == '6' || lines[i].at(2) == '2' || lines[i].at(2) == '4' || lines[i].at(2) == '5'))
		{
			curr = MIN_LENGTH;
		}
		else
		{
			curr = MIN_LENGTH - 1;
		}
		eventId = lines[i].substr(0, curr);
		scanResult.second = scanResult.second + events[eventId] + "\n";
		if (lines[i].length() > curr)
		{
			scanResult.second += lines[i].substr(curr) + "\n";
		}
		if (eventId.at(0) == '0')
		{
			scanResult.second += "\nTherefore, the file is malicious\n";
			scanResult.first = true;
			return scanResult;
		}
		else
		{
			eventsCounter[eventId]++;
		}
	}
	//Event viewer backup log more than one time
	if (eventsCounter["2"] > 1)
	{
		scanResult.second += "\nThe file tried to delete backup log and therefore, the file is ransomware\n";
		scanResult.first = true;
		return scanResult;
	}
	//(download from internet(27) or install(21) or install in documents(22) or url download(17)) & (create file(4) or openFile(11))
	if ((eventsCounter["27"] || eventsCounter["21"] || eventsCounter["22"] || eventsCounter["17"]) && (eventsCounter["4"] || eventsCounter["11"]))
	{
		scanResult.second += "\nThe file tried to install rootkit and therefore, the file is malicious\n";
		scanResult.first = true;
		return scanResult;
	}
	//(createThread(9) or create process(6)) & pe(7)
	if ((eventsCounter["9"] || eventsCounter["6"]) && (eventsCounter["7"]))
	{
		scanResult.second += "\nTherefore, the file is malicious\n";
		scanResult.first = true;
		return scanResult;
	}
	//createRemoteThread(8) & (writeProcessMemory(19) or ReadProcessMemory(13) or openProcess(12) or virtualProtect(18)) at least 2
	if ((eventsCounter["8"] >= 2) && (eventsCounter["19"] >= 2 || eventsCounter["13"] >= 2 || eventsCounter["12"] >= 2 || eventsCounter["18"] >= 2))
	{
		scanResult.second += "\nTherefore, the file is injector\n";
		scanResult.first = true;
		return scanResult;
	}
	//RegCreateKey(14)  & RegOpenKey(15) at least 5 times
	if ((eventsCounter["14"] >= 5) && (eventsCounter["15"] >= 5))
	{
		scanResult.second += "\nTherefore, the file is malicious\n";
		scanResult.first = true;
		return scanResult;
	}
	scanResult.first = false;
	scanResult.second += "\nTherefore, the file is safe\n";
	return scanResult;
}
