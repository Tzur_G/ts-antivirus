#pragma once

#include <iostream>
#include <string>
#include <fstream>
#include <map>
#include <vector>
#include "File.h"
#include "IDatabase.h"
#include "VMHelper.h"
#include "Utilities.h"
#pragma comment (lib, "vix\\vix.lib")

#define VMX_PATH "C:\\magshimim\\Windows10x64\\Windows10x64.vmx"
#define DIRECTORY "c:\\vmTest"
#define LOG_PATH "c:\\vmTest\\log.txt"
#define MIN_LENGTH 3


class DynamicScanHelper
{
public:
	static std::pair<bool, std::string> runDynamicScan(File file, IDatabase* db, bool scanAgain);
private:
	static std::pair<bool, std::string> getReport();

	static std::map<std::string, std::string> events;
												   
	static std::map<std::string, unsigned int> eventsCounter;
};