#include "Encryption.h"

Encryption::Encryption()
{
}

Encryption::~Encryption()
{
}

// Function returns a random string
std::string Encryption::getRandKey()
{
    const char alphnum[] = "0123456789" "!@#$%^&*" "ABCDEFGHIJKLMNOPQRSTUVWXYZ" "abcdefghijklmnopqrstuvwxyz";
    int strLen = sizeof(alphnum) - 1;
    std::string specialChars = "!@#$%^&*";
    int c = 0, s = 0;

Start:
    char ch;
    std::string result;
    for (int i = 0; i < KEY_LEN; i++)
    {
        ch = alphnum[rand() % strLen];
        result += ch;
        if (isdigit(ch))
        {
            c++;
        }
        if (specialChars.find(ch) != std::string::npos)
        {
            s++;
        }
    }
    if (s == 0 || c == 0)
    {
        goto Start;
    }
    return result;
}

bool Encryption::encryptFile(const LPTSTR& pszSourceFile, const LPTSTR& pszDestinationFile, const LPTSTR& pszPassword)
{
    //---------------------------------------------------------------
    // Declare and initialize local variables.
    bool fReturn = false;
    HANDLE hSourceFile = INVALID_HANDLE_VALUE;
    HANDLE hDestinationFile = INVALID_HANDLE_VALUE;

    HCRYPTPROV hCryptProv = NULL;
    HCRYPTKEY hKey = NULL;
    HCRYPTKEY hXchgKey = NULL;
    HCRYPTHASH hHash = NULL;

    PBYTE pbKeyBlob = NULL;
    DWORD dwKeyBlobLen;

    PBYTE pbBuffer = NULL;
    DWORD dwBlockLen;
    DWORD dwBufferLen;
    DWORD dwCount;

    //---------------------------------------------------------------
    // Open the source file. 
    hSourceFile = CreateFile(
        pszSourceFile,
        FILE_READ_DATA,
        FILE_SHARE_READ,
        NULL,
        OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL,
        NULL);
    if (INVALID_HANDLE_VALUE == hSourceFile)
    {
        destroyFunc(&hSourceFile, &hDestinationFile, &pbBuffer, &hCryptProv, &hKey, &hHash);
        return false;
    }

    //---------------------------------------------------------------
    // Open the destination file. 
    hDestinationFile = CreateFile(
        pszDestinationFile,
        FILE_WRITE_DATA,
        FILE_SHARE_READ,
        NULL,
        OPEN_ALWAYS,
        FILE_ATTRIBUTE_NORMAL,
        NULL);
    if (INVALID_HANDLE_VALUE == hDestinationFile)
    {
        destroyFunc(&hSourceFile, &hDestinationFile, &pbBuffer, &hCryptProv, &hKey, &hHash);
        return false;
    }

    //---------------------------------------------------------------
    // Get the handle to the default provider. 
    if (!CryptAcquireContext(
        &hCryptProv,
        NULL,
        MS_ENHANCED_PROV,
        PROV_RSA_FULL,
        CRYPT_VERIFYCONTEXT))
    {
        destroyFunc(&hSourceFile, &hDestinationFile, &pbBuffer, &hCryptProv, &hKey, &hHash);
        return false;
    }

    //-----------------------------------------------------------
    // The file will be encrypted with a session key derived 
    // from a password.
    // The session key will be recreated when the file is 
    // decrypted only if the password used to create the key is 
    // available. 

    //-----------------------------------------------------------
    // Create a hash object. 
    if (!CryptCreateHash(
        hCryptProv,
        CALG_MD5,
        0,
        0,
        &hHash))
    {
        destroyFunc(&hSourceFile, &hDestinationFile, &pbBuffer, &hCryptProv, &hKey, &hHash);
        return false;
    }

    //-----------------------------------------------------------
    // Hash the password. 
    if (!CryptHashData(
        hHash,
        (BYTE*)pszPassword,
        lstrlen(pszPassword),
        0))
    {
        destroyFunc(&hSourceFile, &hDestinationFile, &pbBuffer, &hCryptProv, &hKey, &hHash);
        return false;
    }

    //-----------------------------------------------------------
    // Derive a session key from the hash object. 
    if (!CryptDeriveKey(
        hCryptProv,
        ENCRYPT_ALGORITHM,
        hHash,
        KEYLENGTH,
        &hKey))
    {
        destroyFunc(&hSourceFile, &hDestinationFile, &pbBuffer, &hCryptProv, &hKey, &hHash);
        return false;
    }

    //---------------------------------------------------------------
    // The session key is now ready. If it is not a key derived from 
    // a  password, the session key encrypted with the private key 
    // has been written to the destination file.

    //---------------------------------------------------------------
    // Determine the number of bytes to encrypt at a time. 
    // This must be a multiple of ENCRYPT_BLOCK_SIZE.
    // ENCRYPT_BLOCK_SIZE is set by a #define statement.
    dwBlockLen = 1000 - 1000 % ENCRYPT_BLOCK_SIZE;

    //---------------------------------------------------------------
    // Determine the block size. If a block cipher is used, 
    // it must have room for an extra block. 
    if (ENCRYPT_BLOCK_SIZE > 1)
    {
        dwBufferLen = dwBlockLen + ENCRYPT_BLOCK_SIZE;
    }
    else
    {
        dwBufferLen = dwBlockLen;
    }

    //---------------------------------------------------------------
    // Allocate memory. 
    if (!(pbBuffer = (BYTE*)malloc(dwBufferLen)))
    {
        destroyFunc(&hSourceFile, &hDestinationFile, &pbBuffer, &hCryptProv, &hKey, &hHash);
        return false;
    }

    //---------------------------------------------------------------
    // In a do loop, encrypt the source file, 
    // and write to the source file. 
    bool fEOF = FALSE;
    do
    {
        //-----------------------------------------------------------
        // Read up to dwBlockLen bytes from the source file. 
        if (!ReadFile(
            hSourceFile,
            pbBuffer,
            dwBlockLen,
            &dwCount,
            NULL))
        {
            destroyFunc(&hSourceFile, &hDestinationFile, &pbBuffer, &hCryptProv, &hKey, &hHash);
            return false;
        }

        if (dwCount < dwBlockLen)
        {
            fEOF = TRUE;
        }

        //-----------------------------------------------------------
        // Encrypt data. 
        if (!CryptEncrypt(
            hKey,
            NULL,
            fEOF,
            0,
            pbBuffer,
            &dwCount,
            dwBufferLen))
        {
            destroyFunc(&hSourceFile, &hDestinationFile, &pbBuffer, &hCryptProv, &hKey, &hHash);
            return false;
        }

        //-----------------------------------------------------------
        // Write the encrypted data to the destination file. 
        if (!WriteFile(
            hDestinationFile,
            pbBuffer,
            dwCount,
            &dwCount,
            NULL))
        {
            destroyFunc(&hSourceFile, &hDestinationFile, &pbBuffer, &hCryptProv, &hKey, &hHash);
            return false;
        }

        //-----------------------------------------------------------
        // End the do loop when the last block of the source file 
        // has been read, encrypted, and written to the destination 
        // file.
    } while (!fEOF);

    fReturn = true;
    destroyFunc(&hSourceFile, &hDestinationFile, &pbBuffer, &hCryptProv, &hKey, &hHash);
    DeleteFile(pszSourceFile); // delete decrypted file
    return fReturn;
}


bool Encryption::decryptFile(const LPTSTR& pszSourceFile, const LPTSTR& pszDestinationFile, const LPTSTR& pszPassword)
{
    //---------------------------------------------------------------
    // Declare and initialize local variables.
    bool fReturn = false;
    HANDLE hSourceFile = INVALID_HANDLE_VALUE;
    HANDLE hDestinationFile = INVALID_HANDLE_VALUE;
    HCRYPTKEY hKey = NULL;
    HCRYPTHASH hHash = NULL;

    HCRYPTPROV hCryptProv = NULL;

    DWORD dwCount;
    PBYTE pbBuffer = NULL;
    DWORD dwBlockLen;
    DWORD dwBufferLen;

    //---------------------------------------------------------------
    // Open the source file. 
    hSourceFile = CreateFile(
        pszSourceFile,
        FILE_READ_DATA,
        FILE_SHARE_READ,
        NULL,
        OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL,
        NULL);
    if (INVALID_HANDLE_VALUE == hSourceFile)
    {
        destroyFunc(&hSourceFile, &hDestinationFile, &pbBuffer, &hCryptProv, &hKey, &hHash);
        return false;
    }

    //---------------------------------------------------------------
    // Open the destination file. 
    hDestinationFile = CreateFile(
        pszDestinationFile,
        FILE_WRITE_DATA,
        FILE_SHARE_READ,
        NULL,
        OPEN_ALWAYS,
        FILE_ATTRIBUTE_NORMAL,
        NULL);
    if (INVALID_HANDLE_VALUE == hDestinationFile)
    {
        destroyFunc(&hSourceFile, &hDestinationFile, &pbBuffer, &hCryptProv, &hKey, &hHash);
        return false;
    }

    //---------------------------------------------------------------
    // Get the handle to the default provider. 
    if (!CryptAcquireContext(
        &hCryptProv,
        NULL,
        MS_ENHANCED_PROV,
        PROV_RSA_FULL,
        CRYPT_VERIFYCONTEXT))
    {
        destroyFunc(&hSourceFile, &hDestinationFile, &pbBuffer, &hCryptProv, &hKey, &hHash);
        return false;
    }

    //-----------------------------------------------------------
    // Decrypt the file with a session key derived from a 
    // password. 

    //-----------------------------------------------------------
    // Create a hash object. 
    if (!CryptCreateHash(
        hCryptProv,
        CALG_MD5,
        0,
        0,
        &hHash))
    {
        destroyFunc(&hSourceFile, &hDestinationFile, &pbBuffer, &hCryptProv, &hKey, &hHash);
        return false;
    }

    //-----------------------------------------------------------
    // Hash in the password data. 
    if (!CryptHashData(
        hHash,
        (BYTE*)pszPassword,
        lstrlen(pszPassword),
        0))
    {
        destroyFunc(&hSourceFile, &hDestinationFile, &pbBuffer, &hCryptProv, &hKey, &hHash);
        return false;
    }

    //-----------------------------------------------------------
    // Derive a session key from the hash object. 
    if (!CryptDeriveKey(
        hCryptProv,
        ENCRYPT_ALGORITHM,
        hHash,
        KEYLENGTH,
        &hKey))
    {
        destroyFunc(&hSourceFile, &hDestinationFile, &pbBuffer, &hCryptProv, &hKey, &hHash);
        return false;
    }

    //---------------------------------------------------------------
    // The decryption key is now available, either having been 
    // imported from a BLOB read in from the source file or having 
    // been created by using the password. This point in the program 
    // is not reached if the decryption key is not available.

    //---------------------------------------------------------------
    // Determine the number of bytes to decrypt at a time. 
    // This must be a multiple of ENCRYPT_BLOCK_SIZE. 

    dwBlockLen = 1000 - 1000 % ENCRYPT_BLOCK_SIZE;
    dwBufferLen = dwBlockLen;

    //---------------------------------------------------------------
    // Allocate memory for the file read buffer. 
    if (!(pbBuffer = (PBYTE)malloc(dwBufferLen)))
    {
        destroyFunc(&hSourceFile, &hDestinationFile, &pbBuffer, &hCryptProv, &hKey, &hHash);
        return false;
    }

    //---------------------------------------------------------------
    // Decrypt the source file, and write to the destination file. 
    bool fEOF = false;
    do
    {
        //-----------------------------------------------------------
        // Read up to dwBlockLen bytes from the source file. 
        if (!ReadFile(
            hSourceFile,
            pbBuffer,
            dwBlockLen,
            &dwCount,
            NULL))
        {
            destroyFunc(&hSourceFile, &hDestinationFile, &pbBuffer, &hCryptProv, &hKey, &hHash);
            return false;
        }

        if (dwCount < dwBlockLen)
        {
            fEOF = TRUE;
        }

        //-----------------------------------------------------------
        // Decrypt the block of data. 
        if (!CryptDecrypt(
            hKey,
            0,
            fEOF,
            0,
            pbBuffer,
            &dwCount))
        {
            destroyFunc(&hSourceFile, &hDestinationFile, &pbBuffer, &hCryptProv, &hKey, &hHash);
            return false;
        }

        //-----------------------------------------------------------
        // Write the decrypted data to the destination file. 
        if (!WriteFile(
            hDestinationFile,
            pbBuffer,
            dwCount,
            &dwCount,
            NULL))
        {
            destroyFunc(&hSourceFile, &hDestinationFile, &pbBuffer, &hCryptProv, &hKey, &hHash);
            return false;
        }

        //-----------------------------------------------------------
        // End the do loop when the last block of the source file 
        // has been read, encrypted, and written to the destination 
        // file.
    } while (!fEOF);

    fReturn = true;
    destroyFunc(&hSourceFile, &hDestinationFile, &pbBuffer, &hCryptProv, &hKey, &hHash);

    DeleteFile(pszSourceFile); // delete decrypted file
    return fReturn;
}


// Function releases all memory and closes all handles
void Encryption::destroyFunc(HANDLE* hSourceFile, HANDLE* hDestinationFile, PBYTE* pbBuffer,
    HCRYPTPROV* hCryptProv, HCRYPTKEY* hKey, HCRYPTHASH* hHash)
{
    //---------------------------------------------------------------
    // Close files.
    if (*hSourceFile)
    {
        CloseHandle(*hSourceFile);
    }

    if (*hDestinationFile)
    {
        CloseHandle(*hDestinationFile);
    }

    //---------------------------------------------------------------
    // Free memory. 
    if (*pbBuffer)
    {
        free(*pbBuffer);
    }


    //-----------------------------------------------------------
    // Release the hash object. 
    if (*hHash)
    {
        CryptDestroyHash(*hHash);
        *hHash = NULL;
    }

    //---------------------------------------------------------------
    // Release the session key. 
    if (*hKey)
    {
        CryptDestroyKey(*hKey);
    }

    //---------------------------------------------------------------
    // Release the provider handle. 
    if (*hCryptProv)
    {
        CryptReleaseContext(*hCryptProv, 0);
    }
}