#pragma once
#define _CRT_SECURE_NO_WARNINGS

#include <tchar.h>
#include <stdio.h>
#include <windows.h>
#include <wincrypt.h>
#include <conio.h>
#include <iostream>
#include <string>
#include <cstdlib> 
#include <ctime> 

// Link with the Advapi32.lib file.
#pragma comment (lib, "advapi32")

#define KEYLENGTH  0x00800000
#define ENCRYPT_ALGORITHM CALG_RC4 
#define ENCRYPT_BLOCK_SIZE 8 
#define KEY_LEN 8
#define ENCRYPTED_FILES_DIR "c:\\tsEncryptedFiles"
#define TS_SIGNATURE "TS"

class Encryption
{
public:
	Encryption();
	~Encryption();

	static std::string getRandKey();
	static bool encryptFile(const LPTSTR& source, const LPTSTR& dst, const LPTSTR& key);
	static bool decryptFile(const LPTSTR& source, const LPTSTR& dst, const LPTSTR& key);

private:
	static void destroyFunc(HANDLE* hSourceFile, HANDLE* hDestinationFile, PBYTE* pbBuffer,
		HCRYPTPROV* hCryptProv, HCRYPTKEY* hKey, HCRYPTHASH* hHash);
};
