#include "File.h"

/*Constructor
input: file path
output: nothing
*/
File::File(std::string filePath)
{
	this->filePath = filePath;
	this->fileHash = calculateHash(filePath);
}

/*Destructor
input: none
output: nothing
*/
File::~File()
{
}

/*Getter*/
std::string File::getFilePath() const
{
	return this->filePath;
}

/*Getter*/
std::string File::getFileHash() const
{
	return this->fileHash;
}

/*Getter*/
std::string File::getFileName() const
{
	return this->filePath.substr(this->filePath.find_last_of("\\") + 1);
}

/*Setter*/
void File::setFilePath(const std::string filePath)
{
	this->filePath = filePath;
}

/*Setter*/
void File::setFileHash(const std::string fileHash)
{
	this->fileHash = fileHash;
}

/*Function calculates file hash
input: file path
output: md5 hash
*/
std::string File::calculateHash(std::string filePath)
{
	std::wstring path = std::wstring_convert<std::codecvt_utf8<wchar_t>>().from_bytes(filePath);
	BOOL bResult = FALSE;
	HCRYPTPROV hProv = 0;
	HCRYPTHASH hHash = 0;
	HANDLE hFile = NULL;
	BYTE rgbFile[BUFFER_SIZE];
	DWORD cbRead = 0;
	BYTE rgbHash[MD5LEN];
	DWORD cbHash = 0;
	CHAR rgbDigits[] = "0123456789abcdef";
	LPCWSTR filename = path.c_str();

	hFile = CreateFileW(filename, GENERIC_READ, FILE_SHARE_READ, NULL,
		OPEN_EXISTING, FILE_FLAG_SEQUENTIAL_SCAN, NULL);

	if (INVALID_HANDLE_VALUE == hFile)
		return INVALID;

	if (!CryptAcquireContext(&hProv, NULL, NULL, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT))
	{
		CloseHandle(hFile);
		return INVALID;
	}

	if (!CryptCreateHash(hProv, CALG_MD5, 0, 0, &hHash))
	{
		CloseHandle(hFile);
		CryptReleaseContext(hProv, 0);
		return INVALID;
	}

	while (bResult = ReadFile(hFile, rgbFile, BUFFER_SIZE, &cbRead, NULL))
	{
		if (!cbRead)
			break;

		if (!CryptHashData(hHash, rgbFile, cbRead, 0))
		{
			CryptReleaseContext(hProv, 0);
			CryptDestroyHash(hHash);
			CloseHandle(hFile);
			return INVALID;
		}
	}

	if (!bResult)
	{
		// can't read file
		CryptReleaseContext(hProv, 0);
		CryptDestroyHash(hHash);
		CloseHandle(hFile);
		return INVALID;
	}

	cbHash = MD5LEN;
	if (CryptGetHashParam(hHash, HP_HASHVAL, rgbHash, &cbHash, 0))
	{
		std::string hash;
		for (DWORD i = 0; i < cbHash; i++)
		{
			hash += rgbDigits[rgbHash[i] >> 4];
			hash += rgbDigits[rgbHash[i] & 0xf];
		}

		CloseHandle(hFile);
		return hash;
	}

	CryptDestroyHash(hHash);
	CryptReleaseContext(hProv, 0);
	CloseHandle(hFile);
	return INVALID;

}
