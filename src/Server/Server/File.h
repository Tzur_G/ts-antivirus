#pragma once

#include <iostream>
#include <Windows.h>
#include <Wincrypt.h>
#include <codecvt>

#define BUFFER_SIZE 1024
#define MD5LEN 16
#define INVALID "failed"


class File
{
public: 
	File(std::string filePath);
	~File();
	std::string getFilePath() const;
	std::string getFileHash() const;
	std::string getFileName() const;
	void setFilePath(const std::string filePath);
	void setFileHash(const std::string fileHash);
	static std::string calculateHash(std::string filePath);
private:
	std::string filePath;
	std::string fileHash;
};