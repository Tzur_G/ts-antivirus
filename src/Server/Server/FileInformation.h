#pragma once
#include <iostream>
#include "json.hpp"

using nlohmann::json;

enum ScanTypes
{
    STATIC = 0,
    DYNAMIC = 1,
    BOTH = 2
};

struct FileInformation
{
    std::string path;
    std::string hash;
    std::string report;
    std::string scanDate;
    std::string scanTime;
    bool isMalicious;
    bool isEncrypted;
};

inline void to_json(json& j, const FileInformation& res)
{
	j = json{ {"path", res.path}, {"hash", res.hash}, {"report", res.report}, {"scanDate", res.scanDate}, {"scanTime", res.scanTime}, {"isMalicious", res.isMalicious}, {"isEncrypted", res.isEncrypted} };
}

inline void from_json(const json& j, FileInformation& res)
{
	j.at("path").get_to(res.path);
	j.at("hash").get_to(res.hash);
	j.at("report").get_to(res.report);
	j.at("scanDate").get_to(res.scanDate);
	j.at("scanTime").get_to(res.scanTime);
	j.at("isMalicious").get_to(res.isMalicious);
	j.at("isEncrypted").get_to(res.isEncrypted);
}