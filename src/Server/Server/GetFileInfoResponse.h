#pragma once
#include <iostream>
#include "FileInformation.h"
#include "json.hpp"

using nlohmann::json;

struct GetFileInfoResponse
{
	FileInformation fileInfo;
};

inline void to_json(json& j, const GetFileInfoResponse& res)
{
	j = json{ {"fileInfo", res.fileInfo} };
}

inline void from_json(const json& j, GetFileInfoResponse& res)
{
	j.at("fileInfo").get_to(res.fileInfo);
}