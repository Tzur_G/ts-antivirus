#pragma once
#include <iostream>
#include "json.hpp"

using nlohmann::json;

struct GetFilesResponse
{
	std::vector<std::string> paths;
};

inline void to_json(json& j, const GetFilesResponse& res)
{
	j = json{ {"paths", res.paths} };
}

inline void from_json(const json& j, GetFilesResponse& res)
{
	j.at("paths").get_to(res.paths);
}

