#pragma once
#include <iostream>
#include "json.hpp"

using nlohmann::json;

struct GetStatisticsResponse
{
	unsigned int numOfCleanFiles;
	unsigned int numOfMaliciousFiles;
	unsigned int numOfUserCleanFiles;
	unsigned int numOfUserMaliciousFiles;
	unsigned int numOfUserEncryptedFiles;
};

inline void to_json(json& j, const GetStatisticsResponse& res)
{
	j = json{ {"numOfCleanFiles", res.numOfCleanFiles}, {"numOfMaliciousFiles", res.numOfMaliciousFiles}, {"numOfUserCleanFiles", res.numOfUserCleanFiles}, {"numOfUserMaliciousFiles", res.numOfUserMaliciousFiles}, {"numOfUserEncryptedFiles", res.numOfUserEncryptedFiles} };
}

inline void from_json(const json& j, GetStatisticsResponse& res)
{
	j.at("numOfCleanFiles").get_to(res.numOfCleanFiles);
	j.at("numOfMaliciousFiles").get_to(res.numOfMaliciousFiles);
	j.at("numOfUserCleanFiles").get_to(res.numOfUserCleanFiles);
	j.at("numOfUserMaliciousFiles").get_to(res.numOfUserMaliciousFiles);
	j.at("numOfUserEncryptedFiles").get_to(res.numOfUserEncryptedFiles);
}