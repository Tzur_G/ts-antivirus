#pragma once
#include <iostream>
#include <fstream>
#include <vector>
#include "sqlite3.h"
#include "FileInformation.h"
#define DB_NAME "antivirus.sqlite"


class IDatabase
{
public:
	virtual void open() = 0;
	virtual void close() = 0;
	virtual bool initializeDatabase() = 0;

	virtual void addNewUser(std::string username, std::string password, std::string email) = 0;
	virtual void addNewFileToScans(const int& userId, std::string filePath, bool isMalicious, std::string description, const int& scanType, std::string fileHash) = 0;
	virtual void updateUserDetails(std::string column, std::string newValue, std::string userName) = 0;
	virtual std::string getUserEmail(std::string userName) = 0;

	virtual void decrypyFile(const int& userId, std::string originalPath) = 0;

	virtual bool doesUserExist(std::string username) = 0;
	virtual bool doesHashExist(std::string hash) = 0;
	virtual bool doesEncryptedFileExist(const int& scanId) = 0;

	virtual int getUserId(std::string username) = 0;
	virtual int getScanId() = 0;

	virtual bool doesPasswordMatch(std::string username, std::string password) = 0;
	virtual std::vector<std::string> getPathsOfUsersFiles(int userId, bool onlyEncryptedFiles) = 0;
	virtual FileInformation getFileInformation(int userId, std::string path) = 0;
	virtual std::pair<bool, std::string> getReport(std::string fileHash) = 0;

	virtual unsigned int getNumOfTotalMaliciousFiles() = 0;
	virtual unsigned int getNumOfTotalCleanFiles() = 0;
	virtual unsigned int getNumOfUserMaliciousFiles(const int& userId) = 0;
	virtual unsigned int getNumOfUserCleanFiles(const int& userId) = 0;
	virtual unsigned int getNumOfEncryptedFiles(const int& userId) = 0;

	virtual ~IDatabase();

private:
	virtual bool initializeUsersTable() = 0;
	virtual bool initializeScansTable() = 0;
	virtual bool initializeEncryptedFilesTable() = 0;
	virtual void addNewEncryptedFile(const int& scanId, std::string filePath, const std::string& key) = 0;
	virtual void deleteEncryptedFileFromDB(const int& scanId) = 0;
};

