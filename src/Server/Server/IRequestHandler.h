#pragma once
#include <iostream>
#include "RequestInfo.h"
#include "RequestResult.h"

struct RequestResult;

class IRequestHandler
{
public:
	virtual bool isRequestRelevant(RequestInfo RI) = 0;
	virtual RequestResult handleRequest(RequestInfo RI) = 0;
	virtual void logout(){};
};