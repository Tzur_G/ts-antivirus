#include "JsonRequestPacketDeserializer.h"

using nlohmann::json;

LoginRequest JsonRequestPacketDeserializer::deserializerLoginRequest(std::vector<std::uint8_t> buffer)
{
	LoginRequest login;
	std::string str_buffer(buffer.begin() + 5, buffer.end());
	json j = json::parse(str_buffer);
	login.username = j["username"].get<std::string>();
	login.password = j["password"].get<std::string>();
	return login;
}

SignupRequest JsonRequestPacketDeserializer::deserializerSignupRequest(std::vector<std::uint8_t> buffer)
{
	SignupRequest signup;
	std::string str_buffer(buffer.begin() + 5, buffer.end());
	json j = json::parse(str_buffer);
	signup.username = j["username"].get<std::string>();
	signup.password = j["password"].get<std::string>();
	signup.email = j["email"].get<std::string>();
	signup.reset = j["reset"].get<bool>();
	return signup;
}

ScanRequest JsonRequestPacketDeserializer::deserializerScanRequest(std::vector<std::uint8_t> buffer)
{
	ScanRequest scan;
	std::string str_buffer(buffer.begin() + 5, buffer.end());
	json j = json::parse(str_buffer);
	scan.scanType = j["scanType"].get<unsigned int>();
	scan.path = j["path"].get<std::string>();
	scan.scanAgain = j["scanAgain"].get<bool>();
	return scan;
}

SettingsRequest JsonRequestPacketDeserializer::deserializerSettingsRequest(std::vector<std::uint8_t> buffer)
{
	SettingsRequest settings;
	std::string str_buffer(buffer.begin() + 5, buffer.end());
	json j = json::parse(str_buffer);
	settings.inputType = j["inputType"].get<unsigned int>();
	settings.input = j["input"].get<std::string>();
	return settings;
}

GetFilesRequest JsonRequestPacketDeserializer::deserializerGetFilesRequest(std::vector<std::uint8_t> buffer)
{
	GetFilesRequest getFiles;
	std::string str_buffer(buffer.begin() + 5, buffer.end());
	json j = json::parse(str_buffer);
	getFiles.onlyEncrypted = j["onlyEncrypted"].get<bool>();
	return getFiles;
}

GetFileInfoRequest JsonRequestPacketDeserializer::deserializerGetFileInfoRequest(std::vector<std::uint8_t> buffer)
{
	GetFileInfoRequest fileInfo;
	std::string str_buffer(buffer.begin() + 5, buffer.end());
	json j = json::parse(str_buffer);
	fileInfo.path = j["path"].get<std::string>();
	return fileInfo;
}

DecryptedFileRequest JsonRequestPacketDeserializer::deserializerDecryptedFileRequest(std::vector<std::uint8_t> buffer)
{
	DecryptedFileRequest decryptFile;
	std::string str_buffer(buffer.begin() + 5, buffer.end());
	json j = json::parse(str_buffer);
	decryptFile.path = j["path"].get<std::string>();
	return decryptFile;
}
