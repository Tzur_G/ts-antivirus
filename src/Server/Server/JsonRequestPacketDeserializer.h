#pragma once
#include <iostream>
#include "json.hpp"
#include "LoginRequest.h"
#include "SignupRequest.h"
#include "ScanRequest.h"
#include "SettingsRequest.h"
#include "GetFilesRequest.h"
#include "GetFileInfoRequest.h"
#include "DecryptedFileRequest.h"

class JsonRequestPacketDeserializer
{
public:
	static LoginRequest deserializerLoginRequest(std::vector<std::uint8_t> buffer);
	static SignupRequest deserializerSignupRequest(std::vector<std::uint8_t> buffer);
	static ScanRequest deserializerScanRequest(std::vector<std::uint8_t> buffer);
	static SettingsRequest deserializerSettingsRequest(std::vector<std::uint8_t> buffer);
	static GetFilesRequest deserializerGetFilesRequest(std::vector<std::uint8_t> buffer);
	static GetFileInfoRequest deserializerGetFileInfoRequest(std::vector<std::uint8_t> buffer);
	static DecryptedFileRequest deserializerDecryptedFileRequest(std::vector<std::uint8_t> buffer);
};