#include "JsonResponsePacketSerializer.h"

std::vector<std::uint8_t> JsonResponsePacketSerializer::createBuffer(std::string message, unsigned char code)
{
	std::vector<std::uint8_t> buffer;
	buffer.push_back(code);
	unsigned char length[4];
	int size = message.size();
	memcpy(length, &size, sizeof size);
	for (int i = 0; i < DATA_LENGTH; i++)
	{
		buffer.push_back(length[i]);
	}
	for (int i = 0; i < size; i++)
	{
		buffer.push_back(message[i]);
	}
	return buffer;
}

std::vector<std::uint8_t> JsonResponsePacketSerializer::serializeResponse(ErrorResponse response)
{
	json j = response;
	std::string message = j.dump();
	return createBuffer(message, ERROR_CODE);
}

std::vector<std::uint8_t> JsonResponsePacketSerializer::serializeResponse(LoginResponse response)
{
	json j = response;
	std::string message = j.dump();
	return createBuffer(message, LOGIN_CODE);
}

std::vector<std::uint8_t> JsonResponsePacketSerializer::serializeResponse(SignupResponse response)
{
	json j = response;
	std::string message = j.dump();
	return createBuffer(message, SIGNUP_CODE);
}

std::vector<std::uint8_t> JsonResponsePacketSerializer::serializeResponse(LogoutResponse response)
{
	json j = response;
	std::string message = j.dump();
	return createBuffer(message, LOGOUT_CODE);
}

std::vector<std::uint8_t> JsonResponsePacketSerializer::serializeResponse(ScanResponse response)
{
	json j = response;
	std::string message = j.dump();
	return createBuffer(message, SCAN_CODE);
}

std::vector<std::uint8_t> JsonResponsePacketSerializer::serializeResponse(SettingsResponse response)
{
	json j = response;
	std::string message = j.dump();
	return createBuffer(message, SETTINGS_CODE);
}

std::vector<std::uint8_t> JsonResponsePacketSerializer::serializeResponse(GetFilesResponse response)
{
	json j = response;
	std::string message = j.dump();
	return createBuffer(message, GET_FILES_CODE);
}

std::vector<std::uint8_t> JsonResponsePacketSerializer::serializeResponse(GetFileInfoResponse response)
{
	json j = response;
	std::string message = j.dump();
	return createBuffer(message, GET_FILE_INFO_CODE);
}

std::vector<std::uint8_t> JsonResponsePacketSerializer::serializeResponse(DecryptedFileResponse response)
{
	json j = response;
	std::string message = j.dump();
	return createBuffer(message, DECRYPT_FILE_CODE);
}

std::vector<std::uint8_t> JsonResponsePacketSerializer::serializeResponse(GetStatisticsResponse response)
{
	json j = response;
	std::string message = j.dump();
	return createBuffer(message, GET_STATISTICS_CODE);
}
