#pragma once
#include <iostream>
#include<vector>
#include "ErrorResponse.h"
#include "LoginResponse.h"
#include "SignupResponse.h"
#include "LogoutResponse.h"
#include "ScanResponse.h"
#include "SettingsResponse.h"
#include "GetFilesResponse.h"
#include "GetFileInfoResponse.h"
#include "DecryptedFileResponse.h"
#include "GetStatisticsResponse.h"
#include "Protocol.h"

class JsonResponsePacketSerializer
{
public:
	static std::vector<std::uint8_t> serializeResponse(ErrorResponse response);
	static std::vector<std::uint8_t> serializeResponse(LoginResponse response);
	static std::vector<std::uint8_t> serializeResponse(SignupResponse response);
	static std::vector<std::uint8_t> serializeResponse(LogoutResponse response);
	static std::vector<std::uint8_t> serializeResponse(ScanResponse response);
	static std::vector<std::uint8_t> serializeResponse(SettingsResponse response);
	static std::vector<std::uint8_t> serializeResponse(GetFilesResponse response);
	static std::vector<std::uint8_t> serializeResponse(GetFileInfoResponse response);
	static std::vector<std::uint8_t> serializeResponse(DecryptedFileResponse response);
	static std::vector<std::uint8_t> serializeResponse(GetStatisticsResponse response);
private:
	static std::vector<std::uint8_t> createBuffer(std::string message, unsigned char code);
};
