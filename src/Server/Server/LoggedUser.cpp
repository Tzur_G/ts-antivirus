#include "LoggedUser.h"

/*Constructor
input: user name
output: nothing
*/
LoggedUser::LoggedUser(std::string username)
{
	m_username = username;
}

/*Getter*/
std::string LoggedUser::getUsername() const
{
	return m_username;
}

bool LoggedUser::operator<(const LoggedUser& other) const
{
	return this->m_username < other.getUsername();
}

/*Setter*/
void LoggedUser::setUserName(const std::string name)
{
	this->m_username = name;
}
