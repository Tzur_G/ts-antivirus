#include "LoginManager.h"

LoginManager::LoginManager(IDatabase* db) : m_database(db)
{
}

LoginManager::~LoginManager()
{
}

void LoginManager::signup(std::string username, std::string password, std::string email)
{
	m_database->addNewUser(username, password, email);
	m_loggedUsers.push_back(LoggedUser(username));
}

void LoginManager::login(std::string username, std::string password)
{
	if (isUserLoggedIn(username))
	{
		throw std::string("user is already logged in");
	}
	if (!m_database->doesUserExist(username))
	{
		throw std::string("user doesn't exist");
	}
	if (m_database->doesPasswordMatch(username, password))
	{
		LoggedUser user(username);
		m_loggedUsers.push_back(user);
	}
	else
	{
		throw std::string("Password is incorrect");
	}
}

void LoginManager::logout(std::string username)
{
	if (m_database->doesUserExist(username) && isUserLoggedIn(username))
	{
		std::vector<LoggedUser>::iterator it = m_loggedUsers.begin();
		while (it != m_loggedUsers.end())
		{
			if (it->getUsername() == username)
			{
				m_loggedUsers.erase(it);
				return;
			}
			++it;
		}
	}
	else
	{
		throw std::string("User is not connected");
	}
}

bool LoginManager::isUserLoggedIn(std::string username)
{
	std::vector<LoggedUser>::iterator it = m_loggedUsers.begin();
	while (it != m_loggedUsers.end())
	{
		if (it->getUsername() == username)
		{
			return true;
		}
		it++;
	}
	return false;
}

void LoginManager::updateDetails(unsigned int choice, std::string input, LoggedUser& user)
{
	Utilities::replace(input, "\"", "\"\"");
	if (choice == InputTypes::USERNAME) //change user name
	{
		if (m_database->doesUserExist(input))
		{
			throw std::string("User already exists");
		}
		logout(user.getUsername());
		m_database->updateUserDetails("USERNAME", input, user.getUsername());	
		user.setUserName(input);
		m_loggedUsers.push_back(user);
	}
	else if (choice == InputTypes::PASSWORD) //change password
	{
		std::string hash = Utilities::calculateHashOfString(input);
		if (hash == std::string("failed"))
		{
			throw std::string("cannot make hash to password");
		}
		m_database->updateUserDetails("PASSWORD", hash, user.getUsername());
	}
	else if (choice == InputTypes::EMAIL) //change email
	{
		m_database->updateUserDetails("EMAIL", input, user.getUsername());
	}
	else
	{
		throw std::string("Invalid choice");
	}
}

/*Function checks if user's email is the same as the email that he entered
input: email
output: 1 - valid, 0 - invalid
*/
int LoginManager::emailValidation(std::string userName, std::string email)
{
	if (isUserLoggedIn(userName))
	{
		return 0;
	}
	return m_database->getUserEmail(userName) == email;
}