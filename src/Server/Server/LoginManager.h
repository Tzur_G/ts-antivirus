#pragma once
#include <iostream>
#include <vector>
#include "IDatabase.h"
#include "LoggedUser.h"
#include "SettingsRequest.h"
#include "Utilities.h"

class LoginManager
{
public:
	LoginManager(IDatabase* db);
	~LoginManager();
	void signup(std::string username, std::string password, std::string email);
	void login(std::string username, std::string password);
	void logout(std::string username);
	bool isUserLoggedIn(std::string username);
	void updateDetails(unsigned int choice, std::string input, LoggedUser& user);
	int emailValidation(std::string userName, std::string email);
private:
	IDatabase* m_database;
	std::vector<LoggedUser> m_loggedUsers;
};