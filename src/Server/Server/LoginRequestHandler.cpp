#include "LoginRequestHandler.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"


LoginRequestHandler::LoginRequestHandler(RequestHandlerFactory& handleFactory) : m_handlerFactory(handleFactory)
{

}

bool LoginRequestHandler::isRequestRelevant(RequestInfo RI)
{
	if (RI.id == LOGIN_CODE || RI.id == SIGNUP_CODE)
	{
		return true;
	}
	return false;
}

RequestResult LoginRequestHandler::login(RequestInfo RI)
{
	LoginRequest lr = JsonRequestPacketDeserializer::deserializerLoginRequest(RI.buffer);
	RequestResult result;
	ErrorResponse error;
	try
	{
		m_handlerFactory.getLoginManager().login(lr.username, lr.password);
	}
	catch (const std::string & e)
	{
		error.message = e;
		result.response = JsonResponsePacketSerializer::serializeResponse(error);
		result.newHandler = new LoginRequestHandler(m_handlerFactory.createLoginRequestHandler());
		return result;
	}
	LoginResponse res;
	res.status = 1;
	result.response = JsonResponsePacketSerializer::serializeResponse(res);
	result.newHandler = new MenuRequestHandler(m_handlerFactory.createMenuRequestHandler(LoggedUser(lr.username)));
	return result;
}

RequestResult LoginRequestHandler::signup(RequestInfo RI)
{
	RequestResult result;
	SignupRequest sr = JsonRequestPacketDeserializer::deserializerSignupRequest(RI.buffer);
	SignupResponse res;
	if (sr.reset)
	{
		if (sr.password == "")
		{
			res.status = m_handlerFactory.getLoginManager().emailValidation(sr.username, sr.email);
			result.newHandler = this;
		}
		else
		{
			LoggedUser user(sr.username);
			try
			{
				m_handlerFactory.getLoginManager().updateDetails(1, sr.password, user);
				m_handlerFactory.getLoginManager().login(sr.username, sr.password);
				res.status = 1;
				result.newHandler = new MenuRequestHandler(m_handlerFactory.createMenuRequestHandler(user));
			}
			catch (const std::string & e)
			{
				res.status = 0;
				result.newHandler = this;
			}
		}
	}
	else
	{
		try
		{
			m_handlerFactory.getLoginManager().signup(sr.username, sr.password, sr.email);
			res.status = 1;
			result.newHandler = new MenuRequestHandler(m_handlerFactory.createMenuRequestHandler(LoggedUser(sr.username)));
		}
		catch (const std::string & e)
		{
			ErrorResponse error;
			error.message = e;
			result.response = JsonResponsePacketSerializer::serializeResponse(error);
			result.newHandler = new LoginRequestHandler(m_handlerFactory.createLoginRequestHandler());
			return result;
		}
	}
	result.response = JsonResponsePacketSerializer::serializeResponse(res);
	return result;
}

RequestResult LoginRequestHandler::handleRequest(RequestInfo RI)
{
	if (RI.id == LOGIN_CODE)
	{
		return login(RI);
	}
	else if (RI.id == SIGNUP_CODE)
	{
		return signup(RI);
	}
	ErrorResponse ER;
	ER.message = "ERROR";
	RequestResult result;
	result.response = JsonResponsePacketSerializer::serializeResponse(ER);
	result.newHandler = nullptr;
	return result;
}