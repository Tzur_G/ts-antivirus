#pragma once
#include "IRequestHandler.h"
#include "Protocol.h"
#include "RequestHandlerFactory.h"
#include <iostream>

class RequestHandlerFactory;

class LoginRequestHandler : public IRequestHandler
{
private:
	RequestHandlerFactory& m_handlerFactory;
	RequestResult login(RequestInfo RI);
	RequestResult signup(RequestInfo RI);
public:
	bool isRequestRelevant(RequestInfo RI);
	RequestResult handleRequest(RequestInfo RI);
	LoginRequestHandler(RequestHandlerFactory& handleFactory); //& added
};
