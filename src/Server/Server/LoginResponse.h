#pragma once
#include <iostream>
#include "json.hpp"

using nlohmann::json;

struct LoginResponse
{
	unsigned int status;
};

inline void to_json(json& j, const LoginResponse& res)
{
	j = json{ {"status", res.status} };
}

inline void from_json(const json& j, LoginResponse& res)
{
	j.at("status").get_to(res.status);
}