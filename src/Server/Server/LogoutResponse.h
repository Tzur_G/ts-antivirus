#pragma once

#include <iostream>
#include "json.hpp"

using nlohmann::json;

struct LogoutResponse
{
	unsigned int status;
};

inline void to_json(json& j, const LogoutResponse& res)
{
	j = json{ {"status", res.status} };
}

inline void from_json(const json& j, LogoutResponse& res)
{
	j.at("status").get_to(res.status);
}