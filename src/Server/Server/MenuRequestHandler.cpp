#include "MenuRequestHandler.h"


/*Contructor
input: user and handler factory
output: nothing
*/
MenuRequestHandler::MenuRequestHandler(LoggedUser user, RequestHandlerFactory& handleFactory) : m_user(user), m_handlerFactory(handleFactory)
{
}

bool MenuRequestHandler::isRequestRelevant(RequestInfo RI)
{
	if (RI.id >= LOGOUT_CODE && RI.id <= GET_STATISTICS_CODE)
	{
		return true;
	}
	return false;
}

RequestResult MenuRequestHandler::handleRequest(RequestInfo RI)
{
	switch (RI.id)
	{
	case LOGOUT_CODE:
		return signout(RI);
	case SCAN_CODE:
		return scanning(RI);
	case SETTINGS_CODE:
		return settings(RI); //check email validation in client(send code to mail and then check it)
	case GET_FILES_CODE:
		return getFiles(RI);
	case GET_FILE_INFO_CODE:
		return getFileInfo(RI);
	case DECRYPT_FILE_CODE:
		return decryptFile(RI);
	case GET_STATISTICS_CODE:
		return getStatistics(RI);
	default:
		ErrorResponse ER;
		ER.message = "ERROR";
		RequestResult result;
		result.response = JsonResponsePacketSerializer::serializeResponse(ER);
		result.newHandler = nullptr;
		return result;
	}
}

RequestResult MenuRequestHandler::signout(RequestInfo RI)
{
	RequestResult result;
	try
	{
		m_handlerFactory.getLoginManager().logout(m_user.getUsername());
	}
	catch (const std::string& e)
	{
		ErrorResponse error;
		error.message = e;
		result.response = JsonResponsePacketSerializer::serializeResponse(error);
		result.newHandler = nullptr;
		return result;
	}
	LogoutResponse res;
	res.status = 1;
	result.response = JsonResponsePacketSerializer::serializeResponse(res);
	result.newHandler = new LoginRequestHandler(m_handlerFactory.createLoginRequestHandler());
	return result;
}

void MenuRequestHandler::logout()
{
	try
	{
		m_handlerFactory.getLoginManager().logout(m_user.getUsername());
	}
	catch (const std::string& e)
	{

	}
}

RequestResult MenuRequestHandler::scanning(RequestInfo RI)
{
	ScanRequest sr = JsonRequestPacketDeserializer::deserializerScanRequest(RI.buffer);
	RequestResult result;
	std::pair<bool, std::string> scanResult;
	result.newHandler = this;
	ScanResponse res;
	File scanFile(sr.path);
	json jsonReport;

	try
	{
		if ((sr.scanType == ScanTypes::STATIC) || (sr.scanType == ScanTypes::BOTH)) // static or both
		{
			scanResult = StaticScanHelper::runStaticInvestigate(scanFile, m_handlerFactory.getDataBase(), sr.scanAgain);
			jsonReport["s"] = scanResult.second;
		}
		if (sr.scanType == ScanTypes::DYNAMIC || (sr.scanType == ScanTypes::BOTH && !scanResult.first)) //dynamic or both
		{
			scanResult = DynamicScanHelper::runDynamicScan(scanFile, m_handlerFactory.getDataBase(), sr.scanAgain);
			jsonReport["d"] = scanResult.second;
		}
		scanResult.second = jsonReport.dump();
	}
	catch (const std::pair<bool, std::string>& e)
	{
		if (e.second == std::string("An error occurred during the static scanning") || e.second.substr(0, 5) == std::string("ERROR"))
		{
			ErrorResponse error;
			error.message = e.second;
			result.response = JsonResponsePacketSerializer::serializeResponse(error);
			return result;
		}
		res.status = 2;
		res.report = e.second;
		Utilities::replace(res.report, "\\\\n", "\n");
		res.isMalicious = e.first;
		result.response = JsonResponsePacketSerializer::serializeResponse(res);
		return result;
	}
	catch (const std::string& e)
	{
		ErrorResponse error;
		error.message = e;
		result.response = JsonResponsePacketSerializer::serializeResponse(error);
		return result;
	}
	m_handlerFactory.getDataBase()->addNewFileToScans(m_handlerFactory.getDataBase()->getUserId(this->m_user.getUsername()), sr.path, scanResult.first, scanResult.second, sr.scanType, scanFile.getFileHash());
	Utilities::replace(scanResult.second, "\\\\n", "\n");
	res.status = 1;
	res.report = scanResult.second;
	res.isMalicious = scanResult.first;
	result.response = JsonResponsePacketSerializer::serializeResponse(res);
	return result;
}

RequestResult MenuRequestHandler::settings(RequestInfo RI)
{
	RequestResult result;
	SettingsRequest sr = JsonRequestPacketDeserializer::deserializerSettingsRequest(RI.buffer);
	result.newHandler = this;
	try
	{
		m_handlerFactory.getLoginManager().updateDetails(sr.inputType, sr.input, m_user);
	}
	catch (const std::string& e)
	{
		ErrorResponse error;
		error.message = e;
		result.response = JsonResponsePacketSerializer::serializeResponse(error);
		return result;
	}
	SettingsResponse res;
	res.status = 1;
	result.response = JsonResponsePacketSerializer::serializeResponse(res);
	return result;
}

RequestResult MenuRequestHandler::getFiles(RequestInfo RI)
{
	RequestResult result;
	GetFilesRequest gfr = JsonRequestPacketDeserializer::deserializerGetFilesRequest(RI.buffer);
	GetFilesResponse res;
	res.paths = m_handlerFactory.getDataBase()->getPathsOfUsersFiles(m_handlerFactory.getDataBase()->getUserId(m_user.getUsername()), gfr.onlyEncrypted);
	result.response = JsonResponsePacketSerializer::serializeResponse(res);
	result.newHandler = this;
	return result;
}

RequestResult MenuRequestHandler::getFileInfo(RequestInfo RI)
{
	RequestResult result;
	GetFileInfoRequest gfir = JsonRequestPacketDeserializer::deserializerGetFileInfoRequest(RI.buffer);
	GetFileInfoResponse res;
	res.fileInfo = m_handlerFactory.getDataBase()->getFileInformation(m_handlerFactory.getDataBase()->getUserId(m_user.getUsername()), gfir.path);
	result.response = JsonResponsePacketSerializer::serializeResponse(res);
	result.newHandler = this;
	return result;
}

RequestResult MenuRequestHandler::decryptFile(RequestInfo RI)
{
	RequestResult result;
	DecryptedFileRequest dfr = JsonRequestPacketDeserializer::deserializerDecryptedFileRequest(RI.buffer);
	DecryptedFileResponse res;
	result.newHandler = this;
	try
	{
		m_handlerFactory.getDataBase()->decrypyFile(m_handlerFactory.getDataBase()->getUserId(m_user.getUsername()), dfr.path);
	}
	catch (const std::string& e)
	{
		ErrorResponse error;
		error.message = e;
		result.response = JsonResponsePacketSerializer::serializeResponse(error);
		return result;
	}

	res.status = 1;
	result.response = JsonResponsePacketSerializer::serializeResponse(res);
	return result;
}

RequestResult MenuRequestHandler::getStatistics(RequestInfo RI)
{
	RequestResult result;
	result.newHandler = this;
	GetStatisticsResponse res;
	res.numOfCleanFiles = m_handlerFactory.getDataBase()->getNumOfTotalCleanFiles();
	res.numOfMaliciousFiles = m_handlerFactory.getDataBase()->getNumOfTotalMaliciousFiles();
	res.numOfUserCleanFiles = m_handlerFactory.getDataBase()->getNumOfUserCleanFiles(m_handlerFactory.getDataBase()->getUserId(m_user.getUsername()));
	res.numOfUserMaliciousFiles = m_handlerFactory.getDataBase()->getNumOfUserMaliciousFiles(m_handlerFactory.getDataBase()->getUserId(m_user.getUsername()));
	res.numOfUserEncryptedFiles = m_handlerFactory.getDataBase()->getNumOfEncryptedFiles(m_handlerFactory.getDataBase()->getUserId(m_user.getUsername()));
	result.response = JsonResponsePacketSerializer::serializeResponse(res);
	return result;
}
