#pragma once

#include <iostream>
#include "IRequestHandler.h"
#include "LoggedUser.h"
#include "RequestHandlerFactory.h"
#include "Protocol.h"
#include "ErrorResponse.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#include "StaticScanHelper.h"
#include "DynamicScanHelper.h"

class RequestHandlerFactory;

class MenuRequestHandler : public IRequestHandler
{
private:
	LoggedUser m_user;
	RequestHandlerFactory& m_handlerFactory;

	RequestResult signout(RequestInfo RI);
	RequestResult scanning(RequestInfo RI);
	RequestResult settings(RequestInfo RI);
	RequestResult getFiles(RequestInfo RI);
	RequestResult getFileInfo(RequestInfo RI);
	RequestResult decryptFile(RequestInfo RI);
	RequestResult getStatistics(RequestInfo RI);
public:
	MenuRequestHandler(LoggedUser user, RequestHandlerFactory& handleFactory);
	bool isRequestRelevant(RequestInfo RI);
	RequestResult handleRequest(RequestInfo RI);
	void logout();
};