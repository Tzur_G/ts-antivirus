#pragma once

#define DATA_LENGTH 4

#define ERROR_CODE 0
#define LOGIN_CODE 1
#define SIGNUP_CODE 2
#define LOGOUT_CODE 3
#define SCAN_CODE 4
#define SETTINGS_CODE 5
#define GET_FILES_CODE 6
#define GET_FILE_INFO_CODE 7
#define DECRYPT_FILE_CODE 8
#define GET_STATISTICS_CODE 9
