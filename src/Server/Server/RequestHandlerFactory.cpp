#include "RequestHandlerFactory.h"

LoginRequestHandler RequestHandlerFactory::createLoginRequestHandler()
{
	return LoginRequestHandler(*this);
}

LoginManager& RequestHandlerFactory::getLoginManager()
{
	return m_loginManager;
}

RequestHandlerFactory::RequestHandlerFactory(IDatabase* db, LoginManager lm) : m_database(db), m_loginManager(lm)
{

}

RequestHandlerFactory::~RequestHandlerFactory()
{
}

MenuRequestHandler RequestHandlerFactory::createMenuRequestHandler(LoggedUser user)
{
	return MenuRequestHandler(user, *this);
}

IDatabase* RequestHandlerFactory::getDataBase()
{
	return m_database;
}