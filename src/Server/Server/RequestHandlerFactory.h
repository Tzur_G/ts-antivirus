#pragma once
#include "IDatabase.h"
#include "LoginManager.h"
#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"
//#include "StatisticsManager.h"


class LoginRequestHandler;
class MenuRequestHandler;


class RequestHandlerFactory
{
private:
	IDatabase* m_database;
	LoginManager m_loginManager;
public:
	RequestHandlerFactory(IDatabase* db, LoginManager lm);
	~RequestHandlerFactory();
	LoginRequestHandler createLoginRequestHandler();
	MenuRequestHandler createMenuRequestHandler(LoggedUser user);
	LoginManager& getLoginManager();
	IDatabase* getDataBase();
};
