#pragma once
#include <ctime>
#include "json.hpp"

struct RequestInfo
{
	int id;
	time_t receivalTime;
	std::vector<std::uint8_t> buffer;
};