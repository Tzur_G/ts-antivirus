#pragma once
#include "IRequestHandler.h"

class IRequestHandler;

struct RequestResult
{
	std::vector<std::uint8_t> response;
	IRequestHandler* newHandler;
};