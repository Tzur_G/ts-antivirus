#pragma once
#include <iostream>

struct ScanRequest
{
	unsigned int scanType;
	std::string path;
	bool scanAgain;
};