#pragma once
#include <iostream>
#include "json.hpp"

using nlohmann::json;

struct ScanResponse
{
	unsigned int status;
	std::string report;
	bool isMalicious;
};

inline void to_json(json& j, const ScanResponse& res)
{
	j = json{ {"status", res.status}, {"report", res.report}, {"isMalicious", res.isMalicious} };
}

inline void from_json(const json& j, ScanResponse& res)
{
	j.at("status").get_to(res.status);
	j.at("report").get_to(res.report);
	j.at("isMalicious").get_to(res.isMalicious);

}