#include "Server.h"

/*Constructor
input: communicator
output: nothing
*/
Server::Server(Communicator& communicator) : m_communicator(communicator)
{
}

/*Destructor
input: none
output: nothing
*/
Server::~Server()
{
}

/*run the server
input: none
output: none
*/
void Server::run()
{
	m_communicator.bindAndListen();
}
