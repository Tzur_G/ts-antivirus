#pragma once

#include "Communicator.h"

class Server
{
private:
	Communicator m_communicator;
public:
	Server(Communicator& communicator);
	~Server();
	void run();
};
