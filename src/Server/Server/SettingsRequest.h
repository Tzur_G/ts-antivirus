#pragma once
#include <iostream>
#include "json.hpp"

using nlohmann::json;

enum InputTypes
{
	USERNAME = 0,
	PASSWORD = 1,
	EMAIL = 2
};

struct SettingsRequest
{
	unsigned int inputType;
	std::string input;
};