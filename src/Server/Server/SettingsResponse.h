#pragma once
#include <iostream>
#include "json.hpp"

using nlohmann::json;

struct SettingsResponse
{
	unsigned int status;
};

inline void to_json(json& j, const SettingsResponse& res)
{
	j = json{ {"status", res.status} };
}

inline void from_json(const json& j, SettingsResponse& res)
{
	j.at("status").get_to(res.status);
}