#pragma once
#include <iostream>
#include "json.hpp"

using nlohmann::json;

struct SignupResponse
{
	unsigned int status;
};

inline void to_json(json& j, const SignupResponse& res)
{
	j = json{ {"status", res.status} };
}

inline void from_json(const json& j, SignupResponse& res)
{
	j.at("status").get_to(res.status);
}