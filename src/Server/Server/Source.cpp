#pragma comment (lib, "ws2_32.lib")
//#pragma comment(lib, "AVLibs\\yaraDll.lib")
//#pragma comment(lib, "AVLibs\\VirusTotal.lib")

#include "WSAInitializer.h"
#include "Server.h"
#include <iostream>
#include <exception>
#include <thread>
#include "File.h"
#include "IDatabase.h"
#include "SqliteDatabase.h"
#include <time.h>

using std::cout;
using std::endl;
using std::cin;

int main()
{
	srand(0);

	try
	{
		WSAInitializer wsaInit;
		IDatabase* db = new SqliteDatabase();
		db->open();
		if (!db->initializeDatabase()) //initialize tables
		{
			return 1;
		}
		LoginManager loginMan(db);
		RequestHandlerFactory handlerFactory(db, loginMan);
		Communicator communicator(handlerFactory);
		Server myServer(communicator);
		std::thread t(&Server::run, myServer);
		t.detach();
		std::string input;
		while (true)
		{
			std::cout << "please enter EXIT to exit, any other key to stay" << std::endl;
			std::cin >> input;
			if (input == std::string("EXIT"))
			{
				break;
			}
		}
		delete db;
	}
	catch (const std::exception & e)
	{
		std::cout << "Error occured: " << e.what() << std::endl;
	}
	return 0;
}