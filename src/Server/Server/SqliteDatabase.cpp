#include "SqliteDatabase.h"

// constructor
SqliteDatabase::SqliteDatabase()
{
	this->_errMessage = nullptr;
	this->_db = nullptr;
}

// destructor
SqliteDatabase::~SqliteDatabase()
{
}

// create the database file and open connection with database
void SqliteDatabase::open()
{
	std::ifstream databaseFile(DB_NAME);

	if (databaseFile)
		databaseFile.close();

	int res = sqlite3_open(DB_NAME, &this->_db);

	if (res != SQLITE_OK)
	{
		this->_db = nullptr;
		throw std::exception("[ERROR] Could not open database!");
	}
}

// close the connection with the database
void SqliteDatabase::close()
{
	sqlite3_close(this->_db);
}

// designed to check if item exists
int SqliteDatabase::callbackForExistence(void* data, int numFields, char** fields, char** colNames)
{
	return numFields;
}

// callback for sqlite statements that have a response
int SqliteDatabase::callback(void* data, int numFields, char** fields, char** colNames)
{
	table* sqlTable = static_cast<table*>(data);
	try {
		sqlTable->emplace_back(fields, fields + numFields);
	}
	catch (...) {
		return 1;
	}
	return 0;
}

// create all tables (if they are not exist)
bool SqliteDatabase::initializeDatabase()
{
	if (initializeUsersTable() && initializeScansTable() && initializeEncryptedFilesTable())
	{
		return true;
	}
	else
	{
		std::cout << "[ERROR] Table creation failed" << std::endl;
		return false;
	}
}

// create table USERS
bool SqliteDatabase::initializeUsersTable()
{
	const char* sqlStatement = "create table if not exists USERS(USERID integer primary key autoincrement not null,"
		" USERNAME text not null,"
		" PASSWORD text not null,"
		" EMAIL text not null);";

	int res = sqlite3_exec(_db, sqlStatement, nullptr, nullptr, &_errMessage);
	if (res != SQLITE_OK)
		return false;
	return true;
}

// create table SCANS
bool SqliteDatabase::initializeScansTable()
{
	const char* sqlStatement = "create table if not exists SCANS(SCANID integer primary key autoincrement not null,"
		" FILEPATH text not null,"
		" FILEHASH text not null,"
		" USERID integer not null,"
		" SCANDATE text not null,"
		" SCANTIME text not null,"
		" SCANTYPE integer not null,"
		" ISMALICIOUS boolean not null check (ISMALICIOUS in (0,1)),"
		" DESCRIPTION text not null);";

	int res = sqlite3_exec(_db, sqlStatement, nullptr, nullptr, &_errMessage);
	if (res != SQLITE_OK)
		return false;
	return true;
}

// create table ENCRYPTEDFILES
bool SqliteDatabase::initializeEncryptedFilesTable()
{
	const char* sqlStatement = "create table if not exists ENCRYPTEDFILES(SCANID integer primary key not null,"
		" FILEPATH text not null,"
		" KEY text not null,"
		" foreign key(SCANID) references SCANS(SCANID));";

	int res = sqlite3_exec(_db, sqlStatement, nullptr, nullptr, &_errMessage);
	if (res != SQLITE_OK)
		return false;
	return true;
}

// function adds new user (new row) to the USERS
void SqliteDatabase::addNewUser(std::string username, std::string password, std::string email)
{
	// prevent sql injection
	Utilities::replace(username, "\"", "\"\"");
	Utilities::replace(password, "\"", "\"\"");
	Utilities::replace(email, "\"", "\"\"");

	if (doesUserExist(username))
	{
		throw std::string("User already exists");
	}
	std::string hash = Utilities::calculateHashOfString(password);
	if (hash == std::string("failed"))
	{
		throw std::string("cannot make hash to the password");
	}
	std::string sqlStatement = "insert into USERS (EMAIL, USERNAME, PASSWORD) values(\"" +
		email + "\", \"" + username + "\", \"" + hash + "\")";
	int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &_errMessage);
}

// function adds new file (new row) to the SCANS and to the HASHES and ENCRYPTEDFILES as well
void SqliteDatabase::addNewFileToScans(const int& userId, std::string filePath, bool isMalicious, std::string description, const int& scanType, std::string fileHash)
{
	// prevent sql injection
	Utilities::replace(filePath, "\"", "\"\"");
	Utilities::replace(fileHash, "\"", "\"\"");

	std::string scanDate = Utilities::getCurrDate();
	std::string scanTime = Utilities::getCurrTime();

	int scanId = (int)getNumberFromDB("select SCANID from SCANS where FILEHASH=\"" + fileHash + "\""
		" and USERID=" + std::to_string(userId) + ";");
	if (scanId != -1) // if the user already made a scan of this file
	{ // if the file was scanned	
		int oldScanType = (int)getNumberFromDB("select SCANTYPE from SCANS where SCANID = " + std::to_string(scanId) + ";");
		bool newIsMalicious = isMalicious || (int)getNumberFromDB("select ISMALICIOUS from SCANS where SCANID = " + std::to_string(scanId) + ";");
		std::string oldPath = getStringFromDB("select FILEPATH from SCANS where SCANID = " + std::to_string(scanId) + ";");
		std::string oldReport = getStringFromDB("select DESCRIPTION from SCANS where SCANID = " + std::to_string(scanId) + ";");
		std::string scanTypeStr = "";
		if (scanType == ScanTypes::STATIC)
			scanTypeStr = "s";
		else if (scanType == ScanTypes::DYNAMIC)
			scanTypeStr = "d";

		if ((oldScanType == scanType) || (scanType == ScanTypes::BOTH))
		{
			Utilities::replace(description, "\"", "\"\"");

			// update all
			std::string sqlStatement = "update SCANS set"
				" FILEPATH = \"" + filePath + "\","
				" SCANDATE = \"" + scanDate + "\","
				" SCANTIME = \"" + scanTime + "\","
				" SCANTYPE = " + std::to_string(scanType) + ","
				" ISMALICIOUS = " + std::to_string((int)isMalicious) + ","
				" DESCRIPTION = \"" + description + "\""
				" where scanId = " + std::to_string(scanId) + ";";

			int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &_errMessage);

		}
		else if (oldScanType == ScanTypes::BOTH)
		{
			std::string newDescription = Utilities::updateJson(oldReport, scanTypeStr, Utilities::getJsonValue(description, scanTypeStr));
			Utilities::replace(newDescription, "\"", "\"\"");

			std::string sqlStatement = "update SCANS set"
				" FILEPATH = \"" + filePath + "\","
				" SCANDATE = \"" + scanDate + "\","
				" SCANTIME = \"" + scanTime + "\","
				" ISMALICIOUS = " + std::to_string((int)newIsMalicious) + ","
				" DESCRIPTION = \"" + newDescription + "\""
				" where scanId = " + std::to_string(scanId) + ";";

			int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &_errMessage);
		}
		else // if (oldScanType==STATIC && scanType==DYNAMIC) || (oldScanType==DYNAMIC && scanType==STATIC)
		{
			std::string newDescription = Utilities::updateJson(oldReport, scanTypeStr, Utilities::getJsonValue(description, scanTypeStr));
			Utilities::replace(newDescription, "\"", "\"\"");

			std::string sqlStatement = "update SCANS set"
				" FILEPATH = \"" + filePath + "\","
				" SCANDATE = \"" + scanDate + "\","
				" SCANTIME = \"" + scanTime + "\","
				" SCANTYPE = " + std::to_string(ScanTypes::BOTH) + ","
				" ISMALICIOUS = " + std::to_string((int)newIsMalicious) + ","
				" DESCRIPTION = \"" + newDescription + "\""
				" where scanId = " + std::to_string(scanId) + ";";

			int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &_errMessage);
		}

		if (doesEncryptedFileExist(scanId))
			deleteEncryptedFileFromDB(scanId); // because this is not updated
		// update
		if ((int)getNumberFromDB("select ISMALICIOUS from SCANS where SCANID = " + std::to_string(scanId) + "; ") && isMalicious)
			addNewEncryptedFile(scanId, filePath, Encryption::getRandKey());
	}
	else
	{ // if this is the first scan of this file
		Utilities::replace(description, "\"", "\"\"");
		std::string sqlStatement = "insert into SCANS (FILEPATH, FILEHASH, USERID, SCANDATE, SCANTIME, SCANTYPE ,ISMALICIOUS, DESCRIPTION) values(\"" + filePath + "\",\"" +
			fileHash + "\"," + std::to_string(userId) + ",\"" + scanDate + "\", \"" + scanTime + "\"," + std::to_string(scanType) + "," + std::to_string(isMalicious) + ",\"" + description + "\");";
		int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &_errMessage);
		scanId = getScanId();
		if (isMalicious)
			addNewEncryptedFile(scanId, filePath, Encryption::getRandKey()); // it is a harmful file so it needs to be encrypted
	}
}

// function adds new encrypted file (new row) to the ENCRYPTEDFILES
void SqliteDatabase::addNewEncryptedFile(const int& scanId, std::string filePath, const std::string& key)
{
	std::string command = "mkdir " + std::string(ENCRYPTED_FILES_DIR);
	system(command.c_str());
	Utilities::replace(filePath, "\"", "\"\""); // prevent sql injection
	std::string encryptedFilePath = Utilities::findDstName(filePath, ENCRYPTED_FILES_DIR) + ".tsav"; // add our extension

	// Encrypt the file
	if (Encryption::encryptFile((LPTSTR)filePath.c_str(), (LPTSTR)encryptedFilePath.c_str(), (LPTSTR)key.c_str()))
	{ // The file was successfully encrypted
		std::string sqlStatement = "insert into ENCRYPTEDFILES (SCANID, FILEPATH, KEY) values (" + std::to_string(scanId) +
			",\"" + encryptedFilePath + "\",\"" + key + "\");";
		int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &_errMessage);
	}
}

// function deletes a row from ENCRYPTEDFILES
void SqliteDatabase::deleteEncryptedFileFromDB(const int& scanId)
{
	std::string sqlStatement = "delete from ENCRYPTEDFILES where SCANID = " + std::to_string(scanId) + ";";
	int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &_errMessage);
}

// function decrypt file and deletes it from db
void SqliteDatabase::decrypyFile(const int& userId, std::string originalPath)
{
	int scanId = getNumberFromDB("select SCANID from SCANS where USERID=" + std::to_string(userId) +
		" and FILEPATH=\"" + originalPath + "\";");
	std::string encryptedFilePath = getStringFromDB("select FILEPATH from ENCRYPTEDFILES where SCANID=" + std::to_string(scanId) + ";");
	std::string key = getStringFromDB("select KEY from ENCRYPTEDFILES where SCANID=" + std::to_string(scanId) + ";");

	// Decrypt the file
	if (Encryption::decryptFile((LPTSTR)encryptedFilePath.c_str(), (LPTSTR)originalPath.c_str(), (LPTSTR)key.c_str()))
	{ // The file was successfully decrypted
		deleteEncryptedFileFromDB(scanId);
	}
	else
		throw std::string("Can't decrypt the file");
}

// checks if user exist in USERS
bool SqliteDatabase::doesUserExist(std::string username)
{
	Utilities::replace(username, "\"", "\"\""); // prevent sql injection
	std::string sqlStatement = "select USERNAME from USERS where USERNAME = \"" + username + "\";";

	int res = sqlite3_exec(_db, sqlStatement.c_str(), SqliteDatabase::callbackForExistence, nullptr, &_errMessage);
	return res != 0;
}

// checks if hash exist in HASHES
bool SqliteDatabase::doesHashExist(std::string hash)
{
	if (hash == std::string("failed"))
	{
		throw std::string("cannot make hash to the file");
	}
	Utilities::replace(hash, "\"", "\"\""); // prevent sql injection
	std::string sqlStatement = "select FILEHASH from SCANS where FILEHASH = \"" + hash + "\";";

	int res = sqlite3_exec(_db, sqlStatement.c_str(), SqliteDatabase::callbackForExistence, nullptr, &_errMessage);
	return res != 0;
}

// checks if hash exist in HASHES
bool SqliteDatabase::doesEncryptedFileExist(const int& scanId)
{
	std::string sqlStatement = "select SCANID from ENCRYPTEDFILES where SCANID = " + std::to_string(scanId) + ";";

	int res = sqlite3_exec(_db, sqlStatement.c_str(), SqliteDatabase::callbackForExistence, nullptr, &_errMessage);
	return res != 0;
}

// get the id of specific user from USERS
int SqliteDatabase::getUserId(std::string username)
{
	Utilities::replace(username, "\"", "\"\""); // prevent sql injection
	std::string sqlStatement = "select USERID from USERS where USERNAME = \"" + username + "\";";
	return int(getNumberFromDB(sqlStatement));
}

/*Function checks if password matches to the user
input: user name and password
output: true- correct password, false- incorrect password
*/
bool SqliteDatabase::doesPasswordMatch(std::string username, std::string password)
{
	Utilities::replace(username, "\"", "\"\""); // prevent sql injection
	Utilities::replace(password, "\"", "\"\""); // prevent sql injection

	std::string hash = Utilities::calculateHashOfString(password);
	if (hash == std::string("failed"))
	{
		throw std::string("cannot make hash to password");
	}
	std::string sqlStatement = "select PASSWORD from USERS where USERNAME = \"" + username + "\";";
	table param;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), SqliteDatabase::callback, &param, &_errMessage);

	if (hash == param[0][0])
	{
		return true;
	}
	return false;
}

// get the last scan id
int SqliteDatabase::getScanId()
{
	table param;
	std::string sqlStatement = "select max(SCANID) from SCANS;";
	int res = sqlite3_exec(_db, sqlStatement.c_str(), SqliteDatabase::callback, &param, &_errMessage);
	return std::stof(param[0][0]);
}

// for statements intended for response of number
float SqliteDatabase::getNumberFromDB(std::string sqlStatement)
{
	table param;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), SqliteDatabase::callback, &param, &_errMessage);
	if (param.empty())
		return -1;
	else
		return std::stof(param[0][0]);
}

// for statements intended for response of string
std::string SqliteDatabase::getStringFromDB(std::string sqlStatement)
{
	table param;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), SqliteDatabase::callback, &param, &_errMessage);
	if (param.empty())
		return "";
	else
		return param[0][0];
}

// get a list of files paths (it could be only the encrypted ones) of user scans
std::vector<std::string> SqliteDatabase::getPathsOfUsersFiles(int userId, bool onlyEncryptedFiles)
{
	std::string sqlStatement;
	table param;
	std::map<std::string, std::string> datesAndPaths;
	if (onlyEncryptedFiles)
	{
		sqlStatement = "select SCANS.FILEPATH, SCANS.SCANDATE, SCANS.SCANTIME from SCANS inner join ENCRYPTEDFILES"
			" on SCANS.SCANID=ENCRYPTEDFILES.SCANID where ENCRYPTEDFILES.SCANID in (select SCANID from SCANS where USERID=" + std::to_string(userId) + ");";
	}
	else // all scanned files of user
	{
		sqlStatement = "select FILEPATH, SCANDATE, SCANTIME from SCANS where USERID=" + std::to_string(userId) + ";";
	}
	int res = sqlite3_exec(_db, sqlStatement.c_str(), SqliteDatabase::callback, &param, &_errMessage);
	for (int i = 0; i < param.size(); i++)
	{
		datesAndPaths[param[i][1] + "|" + param[i][2] + std::to_string(i)] = param[i][0];
	}

	return Utilities::sortByDate(datesAndPaths); // from the latest to the earliest
}

// get information on user's file by its path
FileInformation SqliteDatabase::getFileInformation(int userId, std::string path)
{
	Utilities::replace(path, "\"", "\"\""); // prevent sql injection

	FileInformation fileInfo = {};
	table param;
	std::string sqlStatement = "select FILEHASH, DESCRIPTION, SCANDATE, SCANTIME, ISMALICIOUS, SCANID from SCANS"
		" where USERID=" + std::to_string(userId) + " and FILEPATH=\"" + path + "\";";
	int res = sqlite3_exec(_db, sqlStatement.c_str(), SqliteDatabase::callback, &param, &_errMessage);
	if (!param.empty())
	{
		fileInfo.path = path;
		fileInfo.hash = param[0][0];
		fileInfo.report = param[0][1];
		Utilities::replace(fileInfo.report, "\\\\n", "\n");
		fileInfo.scanDate = param[0][2];
		fileInfo.scanTime = param[0][3];
		fileInfo.isMalicious = (int)std::stof(param[0][4]);
		fileInfo.isEncrypted = doesEncryptedFileExist(std::stof(param[0][5]));
	}

	return fileInfo;
}

// function returns existed report of file
std::pair<bool, std::string> SqliteDatabase::getReport(std::string fileHash)
{
	Utilities::replace(fileHash, "\"", "\"\""); // prevent sql injection

	std::pair<bool, std::string> report; // report.second = "db{"s": <static report>, "d": <dynamic report>}"
	table param;
	std::string sqlStatement = "select SCANTYPE, ISMALICIOUS, DESCRIPTION from SCANS where FILEHASH = \"" + fileHash + "\";";
	int res = sqlite3_exec(_db, sqlStatement.c_str(), SqliteDatabase::callback, &param, &_errMessage);

	if (!param.empty())
	{
		std::pair<bool, std::string> staticReport; // staticReport.second = {"s": <static report>}
		std::pair<bool, std::string> dynamicReport; // dynamicReport.second = {"d": <dynamic report>}

		for (int i = 0; i < param.size(); i++)
		{
			if (std::stof(param[i][0]) == ScanTypes::BOTH)
			{
				report.first = std::stof(param[i][1]);
				report.second = param[i][2];
				break;
			}
			else if (std::stof(param[i][0]) == ScanTypes::STATIC)
			{
				if (staticReport.second.empty() ||
					(staticReport.second.find("there was no internet connection") != std::string::npos)) // because maybe this scan made with internet
				{
					staticReport.first = std::stof(param[i][1]);
					staticReport.second = param[i][2];
				}
			}
			else
			{
				if (dynamicReport.second.empty())
				{
					dynamicReport.first = std::stof(param[i][1]);
					dynamicReport.second = param[i][2];
				}
			}
		}

		if (report.second.empty())
		{
			if (staticReport.second.empty())
			{
				report.first = dynamicReport.first;
				report.second = dynamicReport.second;
			}
			else if (dynamicReport.second.empty())
			{
				report.first = staticReport.first;
				report.second = staticReport.second;
			}
			else // combine
			{
				report.first = staticReport.first || dynamicReport.first;
				// add the dynamic report to the static report
				report.second = Utilities::updateJson(staticReport.second, "d", dynamicReport.second);
			}
		}

	}

	return report;
}

unsigned int SqliteDatabase::getNumOfTotalMaliciousFiles()
{
	std::string sqlStatement = "select count(distinct FILEHASH) from SCANS where ISMALICIOUS=1;";
	return (unsigned int)this->getNumberFromDB(sqlStatement);
}

unsigned int SqliteDatabase::getNumOfTotalCleanFiles()
{
	std::string sqlStatement = "select count(distinct FILEHASH) from SCANS where ISMALICIOUS=0;";
	return (unsigned int)this->getNumberFromDB(sqlStatement);
}

// returns the number of malicious files that a user scanned
unsigned int SqliteDatabase::getNumOfUserMaliciousFiles(const int& userId)
{
	std::string sqlStatement = "select count(SCANID) from SCANS where USERID=" + std::to_string(userId) + " and ISMALICIOUS=1;";
	return (unsigned int)this->getNumberFromDB(sqlStatement);
}

// returns the number of clean files that a user scanned
unsigned int SqliteDatabase::getNumOfUserCleanFiles(const int& userId)
{
	std::string sqlStatement = "select count(SCANID) from SCANS where USERID=" + std::to_string(userId) + " and ISMALICIOUS=0;";
	return (unsigned int)this->getNumberFromDB(sqlStatement);
}

// returns the number of user's encrypted files
unsigned int SqliteDatabase::getNumOfEncryptedFiles(const int& userId)
{
	std::string sqlStatement = "select count(SCANID) from ENCRYPTEDFILES where SCANID in (select SCANID from SCANS where USERID=" + std::to_string(userId) + ");";
	return (unsigned int)this->getNumberFromDB(sqlStatement);
}

//Function updates user's details according to his choice
void SqliteDatabase::updateUserDetails(std::string column, std::string newValue, std::string userName)
{
	std::string sqlStatement = "update USERS set " + column + " = " + "\"" + newValue + "\" where USERID = " + std::to_string(getUserId(userName)) + ";";
	int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &_errMessage);
}

//function returns user's email
std::string SqliteDatabase::getUserEmail(std::string userName)
{
	Utilities::replace(userName, "\"", "\"\""); // prevent sql injection
	std::string sqlStatement = "select EMAIL from USERS where USERNAME = \"" + userName + "\";";
	return getStringFromDB(sqlStatement);
}