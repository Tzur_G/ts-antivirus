#include <exception>
#include <string>
#include <vector>
#include "IDatabase.h"
#include "Utilities.h"
#include "Encryption.h"

#define MAX_DATA_LENGTH 512

using table = std::vector<std::vector<std::string>>;

class SqliteDatabase : public IDatabase
{
public:
	SqliteDatabase();
	~SqliteDatabase();

	// Initialization
	virtual void open();
	virtual void close();
	virtual bool initializeDatabase();

	// USERS
	virtual void addNewUser(std::string username, std::string password, std::string email);
	virtual bool doesUserExist(std::string username);
	virtual int getUserId(std::string username);
	virtual bool doesPasswordMatch(std::string username, std::string password);
	virtual void updateUserDetails(std::string column, std::string newValue, std::string userName);
	virtual std::string getUserEmail(std::string userName);

	// SCANS
	virtual void addNewFileToScans(const int& userId, std::string filePath, bool isMalicious, std::string description, const int& scanType, std::string fileHash);
	virtual int getScanId();
	virtual std::vector<std::string> getPathsOfUsersFiles(int userId, bool onlyEncryptedFiles);
	virtual FileInformation getFileInformation(int userId, std::string path);
	virtual bool doesHashExist(std::string hash);
	virtual std::pair<bool, std::string> getReport(std::string fileHash);
	virtual unsigned int getNumOfTotalMaliciousFiles();
	virtual unsigned int getNumOfTotalCleanFiles();
	virtual unsigned int getNumOfUserMaliciousFiles(const int& userId);
	virtual unsigned int getNumOfUserCleanFiles(const int& userId);
	
	// ENCRYPTEDFILES
	virtual bool doesEncryptedFileExist(const int& scanId);
	virtual void decrypyFile(const int& userId, std::string originalPath);
	virtual int unsigned getNumOfEncryptedFiles(const int& userId);

private:
	virtual bool initializeUsersTable();
	virtual bool initializeScansTable();
	virtual bool initializeEncryptedFilesTable();
	virtual void addNewEncryptedFile(const int& scanId, std::string filePath, const std::string& key);
	virtual void deleteEncryptedFileFromDB(const int& scanId);

	static int callbackForExistence(void* exists, int argc, char** argv, char** azColName);
	static int callback(void* data, int argc, char** argv, char** azColName);
	float getNumberFromDB(std::string sqlStatement);
	std::string getStringFromDB(std::string sqlStatement);

	sqlite3* _db;
	char* _errMessage;

};