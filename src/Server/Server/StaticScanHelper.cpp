#define _CRT_SECURE_NO_WARNINGS
#include "StaticScanHelper.h"

/*Function is running static investigate
input: file for the scanning and db
ouput: pair that contains false and error occurred or file is safe or true and file is malicious
*/
std::pair<bool, std::string> StaticScanHelper::runStaticInvestigate(File file, IDatabase* db, bool scanAgain)
{

	bool signResult;
	std::ifstream readFile;
	std::ofstream cleanFile;
	int stringsResult = 0;
	std::wstring filePath = std::wstring_convert<std::codecvt_utf8<wchar_t>>().from_bytes(file.getFilePath());
	bool yaraResult;
	bool checkAgain = true;
	std::string finalReport = "";
	bool continueVt = true;

	if (!scanAgain && db->doesHashExist(file.getFileHash())) //if hash exists in db then send scanning report to the user, otherwise continue the scan
	{
		throw db->getReport(file.getFileHash());
	}

	/*vt scan*/

	// check the internet connection (the scan requires an Internet connection)
	if (!InternetCheckConnectionW(L"http://www.google.com", FLAG_ICC_FORCE_CONNECTION, 0))
	{
		//cout << "No internet connection" << endl;
		finalReport = std::string("virus total check: can't check hash in virus total because there was no internet connection\n");
		continueVt = false;
	}

	if (continueVt)
	{
		// load the virus total lib
		HMODULE vtModule = LoadLibraryA(VT_LIB);
		if (NULL == vtModule)
		{
			//cout << "Failed to load virus total DLL" << endl;
			throw std::make_pair(false, "An error occurred during the static scanning");
		}

		VT_FUNC vtFunc = (VT_FUNC)GetProcAddress(vtModule, "getReportFromVirusTotal");
		if (NULL != vtFunc)
		{
			char* report = new char[MAX_REPORT_LEN] {0};
			strncpy(report, (*vtFunc)(filePath.c_str()), MAX_REPORT_LEN);
			FreeLibrary(vtModule);
			std::string vtReport(report);
			if (vtReport == std::string("CLEAN"))
			{
				//cout << "virus total scan: file is safe" << endl;
				finalReport += std::string("virus total scan: file is safe\n");
				checkAgain = false;
			}
			else if (vtReport == std::string("TIMEOUT"))
			{
				//cout << "virus total scan: time out" << endl;
			}
			else if (vtReport == std::string("INVALID PATH"))
			{
				//cout << "invalid path" << endl;
				throw std::make_pair(false, "An error occurred during the static scanning");
			}
			else
			{
				finalReport += "virus total scan: \n" + std::string(report);
				std::string sReport = report;
				if (std::count(sReport.begin(), sReport.end(), '\n') <= 1 &&
					sReport.length() >= 4 && 
					sReport.substr(0, 4) == std::string("APEX"))
				{
					checkAgain = false;
				}
				else
				{
					finalReport += "\nfinal result: file is malicious";
					return std::make_pair(true, finalReport);
					//cout << report << endl;
				}
			}
		}
		else
		{
			//cout << "Failed to load getReportFromVirusTotal function" << endl;
			throw std::make_pair(false, "An error occurred during the static scanning");
		}
	}

	/*yara rules scan*/

	// Load the dll
	HMODULE yaraModule = LoadLibraryA(YARA_LIB);
	if (NULL == yaraModule)
	{
		//cout << "Failed to load yara DLL" << endl;
		throw std::make_pair(false, "An error occurred during the static scanning");
	}

	//get yaraScan function address
	YARA_FUNC yaraFunc = (YARA_FUNC)GetProcAddress(yaraModule, "wrapper");
	if (NULL != yaraFunc)
	{
		yaraResult = yaraFunc(file.getFilePath().c_str());
		FreeLibrary(yaraModule);
		if (yaraResult)
		{
			//cout << "yara rules scan: file is safe" << endl;
			finalReport += std::string("yara rules scan: file is safe\n");
		}
		else
		{
			readFile.open("output.txt"); //get scan result
			std::string output((std::istreambuf_iterator<char>(readFile)), std::istreambuf_iterator<char>());
			readFile.close();
			//cout << output;
			cleanFile.open("output.txt", std::ofstream::out | std::ofstream::trunc); //clean file
			cleanFile.close();
			return std::make_pair(true, finalReport + "\n" + output + "\nfinal result: file is malicious");
		}
	}
	else
	{
		//cout << "Failed to load wrapper function" << endl;
		throw std::make_pair(false, "An error occurred during the static scanning");
	}

	/*strings scan*/

	// Load the dll
	HMODULE stringsModule = LoadLibraryA(STRINGS_LIB);
	if (NULL == stringsModule)
	{
		//cout << "Failed to load strings DLL" << endl;
		throw std::make_pair(false, "An error occurred during the static scanning");
	}

	//get stringsScan function address
	STRINGS_FUNC stringsFunc = (STRINGS_FUNC)GetProcAddress(stringsModule, "stringsScan");
	if (NULL != stringsFunc)
	{
		stringsResult = stringsFunc(file.getFilePath().c_str());
		FreeLibrary(stringsModule);
		if (stringsResult == 0)
		{
			//cout << "strings.exe scan: file is safe" << endl;
			finalReport += std::string("strings.exe scan: file is safe\n");
		}
		else
		{
			//cout << "found " << stringsResult << " suspicious strings:" << endl;
			readFile.open("result.txt"); //get scan result
			std::string output((std::istreambuf_iterator<char>(readFile)), std::istreambuf_iterator<char>());
			readFile.close();
			//cout << output << endl;
			finalReport += std::string("found " + std::to_string(stringsResult) + " suspicious strings:\n");
			finalReport = finalReport + output + "\n";
		}
		cleanFile.open("result.txt", std::ofstream::out | std::ofstream::trunc); //clean file
		cleanFile.close();
	}
	else
	{
		//cout << "Failed to load stringsScan function" << endl;
		throw std::make_pair(false, "An error occurred during the static scanning");
	}

	/*signature scan*/

	// Load the dll
	HMODULE signModule = LoadLibraryA(SIGN_LIB);
	if (NULL == signModule)
	{
		//cout << "Failed to load verify Signature DLL" << endl;
		throw std::make_pair(false, "An error occurred during the static scanning");
	}

	//get VerifyEmbeddedSignature function address
	SIGN_FUNC signFunc = (SIGN_FUNC)GetProcAddress(signModule, "VerifyEmbeddedSignature");
	if (NULL != signFunc)
	{
		signResult = signFunc(filePath.c_str());
		FreeLibrary(signModule);
		if (signResult)
		{
			//cout << "signature scan: file is signed" << endl;
			finalReport += std::string("signature scan: file is signed\n");
		}
		else
		{
			//cout << "signature scan: file isn't signed" << endl;
			finalReport += std::string("signature scan: file isn't signed\n");
		}
	}
	else
	{
		//cout << "Failed to load VerifyEmbeddedSignature function" << endl;
		throw std::make_pair(false, "An error occurred during the static scanning");
	}

	//check if there is report from virus total
	if (checkAgain && continueVt)
	{
		// load the virus total lib
		HMODULE vt2Module = LoadLibraryA(VT_LIB);
		if (NULL == vt2Module)
		{
			//cout << "Failed to load virus total DLL" << endl;
			throw std::make_pair(false, "An error occurred during the static scanning");
		}

		VT_REPORT vt2Func = (VT_REPORT)GetProcAddress(vt2Module, "isVTReportExist");
		if (NULL != vt2Func)
		{
			char* report = new char[MAX_REPORT_LEN] {0};
			strncpy(report, (*vt2Func)(filePath.c_str()), MAX_REPORT_LEN);
			FreeLibrary(vt2Module);
			std::string vtReport(report);
			if (vtReport == std::string("CLEAN"))
			{
				//cout << "virus total scan: file is safe" << endl;
				finalReport = std::string("virus total scan: file is safe\n") + finalReport;
			}
			else if (std::count(vtReport.begin(), vtReport.end(), '\n') <= 1 && vtReport.length() >= 4 && vtReport.substr(0, 4) == std::string("APEX"))
			{
				finalReport = std::string("virus total scan: \n") + vtReport + finalReport;
			}
			else if ((vtReport != std::string("NOT EXISTS")) && (vtReport != std::string("")) && vtReport != std::string("TIMEOUT"))
			{
				//cout << report << endl;
				return std::make_pair(true, "virus total scan: \n" + vtReport + finalReport + "\nfinal result: file is malicious");
			}
		}
		else
		{
			//cout << "Failed to load isVTReportExist function" << endl;
			throw std::make_pair(false, "An error occurred during the static scanning");
		}
	}

	//rate the file according to strings scan and signature scan
	if (!signResult && stringsResult >= 4)
	{
		return std::make_pair(true, finalReport + "\nfinal result: file is malicious");
	}
	return std::make_pair(false, finalReport + "\nfinal result: file is safe");
}