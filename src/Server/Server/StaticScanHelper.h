#pragma once

#include <iostream>
#include <Windows.h>
#include <codecvt>
#include <fstream>
#include <wininet.h>
#include <string>
#include <algorithm>
#include "File.h"
#include "IDatabase.h"
#include "ScanRequest.h"

#pragma comment(lib,"Wininet.lib")

#define SIGN_LIB "AVLibs/VerifySignature.dll"
#define STRINGS_LIB "AVLibs/StringsLib.dll"
#define YARA_LIB "AVLibs/yaraDll.dll"
#define VT_LIB "AVLibs/VirusTotal.dll"

#define MAX_REPORT_LEN 512

typedef BOOL(*SIGN_FUNC)(LPCWSTR);
typedef int(*STRINGS_FUNC)(const char*);
typedef bool(*YARA_FUNC)(const char*);
typedef char* (*VT_FUNC)(const wchar_t*);
typedef char* (*VT_REPORT)(const wchar_t*);


//using std::cout;
//using std::endl;

class StaticScanHelper
{
public:
	static std::pair<bool, std::string> runStaticInvestigate(File file, IDatabase* db, bool scanAgain);
};