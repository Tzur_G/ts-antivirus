#include "Utilities.h"

std::string Utilities::getCurrDate()
{
	const int MAXLEN = 11;
	char s[MAXLEN];
	time_t t = time(0);
	strftime(s, MAXLEN, "%d/%m/%Y", localtime(&t));
	return std::string(s);
}

std::string Utilities::getCurrTime()
{
	time_t timenow = time(0);
	struct tm* localTime;
	localTime = localtime(&timenow);  // convert the current time to the local time
	std::string hour = std::to_string(localTime->tm_hour);
	if (hour.length() == 1)
		hour = "0" + hour;
	std::string minutes = std::to_string(localTime->tm_min);
	if (minutes.length() == 1)
		minutes = "0" + minutes;
	return hour + ":" + minutes;
}

/*Function calculates the hash of a string
input: a string
output: md5 hash
*/
std::string Utilities::calculateHashOfString(std::string str)
{
	HCRYPTPROV hProv = 0;
	HCRYPTHASH hHash = 0;
	BYTE* rgbFile;
	DWORD cbRead = str.size();
	BYTE rgbHash[MD5LEN];
	DWORD cbHash = 0;
	CHAR rgbDigits[] = "0123456789abcdef";

	if (!CryptAcquireContext(&hProv, NULL, NULL, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT))
	{
		return INVALID;
	}

	if (!CryptCreateHash(hProv, CALG_MD5, 0, 0, &hHash))
	{
		CryptReleaseContext(hProv, 0);
		return INVALID;
	}

	std::vector<BYTE> Bytes(cbRead);
	memcpy_s(&Bytes[0], cbRead, (LPCTSTR)(str.c_str()), cbRead);
	rgbFile = Bytes.data();

	if (!CryptHashData(hHash, rgbFile, cbRead, 0))
	{
		CryptReleaseContext(hProv, 0);
		CryptDestroyHash(hHash);
		return INVALID;
	}

	cbHash = MD5LEN;
	if (CryptGetHashParam(hHash, HP_HASHVAL, rgbHash, &cbHash, 0))
	{
		std::string hash;
		for (DWORD i = 0; i < cbHash; i++)
		{
			hash += rgbDigits[rgbHash[i] >> 4];
			hash += rgbDigits[rgbHash[i] & 0xf];
		}

		return hash;
	}

	CryptDestroyHash(hHash);
	CryptReleaseContext(hProv, 0);
	return INVALID;
}

// change value of a key from report
std::string Utilities::updateJson(const std::string& report, std::string key, std::string newValue)
{
	Utilities::replace(key, "\"", "'");
	Utilities::replace(newValue, "\"", "'");

	try
	{
		json jsonReport = json::parse(report);
		jsonReport[key] = newValue;
		return jsonReport.dump();
	}
	catch (...)
	{
		return "{\"" + key + ": " + newValue + "\"}";
	}
}

// get value of a key
std::string Utilities::getJsonValue(const std::string& report, std::string key)
{
	Utilities::replace(key, "\"", "'");

	try
	{
		json jsonReport = json::parse(report);
		std::string val = jsonReport[key].dump().substr(1, jsonReport[key].dump().size() - 2); // remove the first and the last character
		return val;
	}
	catch (...)
	{
		return "";
	}
}

// Convert the string of the date and the time to struct tm
// date : dd/mm/yyyy|HH:MM
tm* Utilities::convertStringToDatetime(const std::string& date)
{
	struct tm* dateTime = new tm();
	std::vector<std::string> temp;

	// convert date
	dateTime->tm_mday = std::stof(std::string(1, date[0]) + date[1]);
	dateTime->tm_mon = std::stof(std::string(1, date[3]) + date[4]) - 1;
	dateTime->tm_year = std::stof(std::string(1, date[6]) + date[7] + std::string(1, date[8]) + date[9]);

	// convert hour
	dateTime->tm_hour = std::stof(std::string(1, date[11]) + date[12]);
	dateTime->tm_min = std::stof(std::string(1, date[14]) + date[15]);

	dateTime->tm_sec = (int)std::stof(std::string(1, date[16])) % 60;

	return dateTime;
}

// get list with string date and string and sort it 
std::vector<std::string> Utilities::sortByDate(std::map<std::string, std::string>& list)
{
	std::vector<std::string> values;
	std::map<tm*, std::string, SortTime> convertedList;
	for (std::pair<std::string, std::string> dateAndStr : list)
		convertedList[Utilities::convertStringToDatetime(dateAndStr.first)] = dateAndStr.second;
	
	for (std::pair<tm*, std::string> dateAndStr : convertedList)
		values.push_back(dateAndStr.second);

	std::reverse(values.begin(), values.end());
	return values;
}

// replace a part of a string with another part
void Utilities::replace(std::string& str, const std::string& from, const std::string& to) 
{
	if (from.empty())
		return;
	size_t start_pos = 0;
	while ((start_pos = str.find(from, start_pos)) != std::string::npos) {
		str.replace(start_pos, from.length(), to);
		start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
	}
}

// Function returns a path that doesn't exist in the encrypted files dir
// source -> path of the source file that about to be encrypted,
// so the destination file name will be similiar to source file name
std::string Utilities::findDstName(const std::string& source, const std::string& dstDir)
{
	size_t dotindex = source.find_last_of(".");
	size_t firstIndex = source.find_last_of("\\") + 1;
	std::string fileName = source.substr(firstIndex, dotindex - firstIndex); // name without file extension
	std::string fileExt = source.substr(dotindex + 1); // file extension
	std::string result = dstDir + std::string("\\") + fileName + "." + fileExt;
	FILE* file = fopen(result.c_str(), "r");

	while (file)
	{
		fclose(file);
		// find another name
		fileName += "%";
		result = dstDir + std::string("\\") + fileName + "." + fileExt;
		file = fopen(result.c_str(), "r");
	}

	return result;
}