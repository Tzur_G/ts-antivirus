#pragma once
#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <string>
#include <ctime>
#include <Windows.h>
#include <Wincrypt.h>
#include <codecvt>
#include <vector>
#include "json.hpp"

using nlohmann::json;

#define BUFFER_SIZE 1024
#define MD5LEN 16
#define INVALID "failed"

class Utilities
{
public:
	static std::string getCurrDate(); // in DD/MM/YY
	static std::string getCurrTime(); // in H:M
	static std::string updateJson(const std::string& report, std::string key, std::string newValue);
	static std::string getJsonValue(const std::string& report, std::string key);
	static std::string calculateHashOfString(std::string str);
	static tm* convertStringToDatetime(const std::string& date);
	static std::vector<std::string> sortByDate(std::map<std::string, std::string>& list);
	static void replace(std::string& str, const std::string& from, const std::string& to);
	static std::string findDstName(const std::string& source, const std::string& dstDir);
};

struct SortTime {
	bool operator() (tm* first, tm* second) const
	{
		if (first->tm_year < second->tm_year)
			return true;
		else if (first->tm_year > second->tm_year)
			return false;

		if (first->tm_mon < second->tm_mon)
			return true;
		else if (first->tm_mon > second->tm_mon)
			return false;

		if (first->tm_mday < second->tm_mday)
			return true;
		else if (first->tm_mday > second->tm_mday)
			return false;

		if (first->tm_hour < second->tm_hour)
			return true;
		else if (first->tm_hour > second->tm_hour)
			return false;

		if (first->tm_min < second->tm_min)
			return true;
		else if (first->tm_min > second->tm_min)
			return false;

		if (first->tm_sec < second->tm_sec)
			return true;
		else if (first->tm_sec > second->tm_sec)
			return false;

		return false;
	}
};