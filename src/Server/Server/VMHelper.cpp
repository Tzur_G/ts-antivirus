#include "VMHelper.h"


/*Constructor
input: the path of the .vmx file
output: nothing
*/
VMHelper::VMHelper(char* vmxPath)
{
	this->_vmxPath = vmxPath;
    this->_hostHandle = VIX_INVALID_HANDLE;
    this->_vmHandle = VIX_INVALID_HANDLE;
}


/*destructor
input: none
output: nothing
*/
VMHelper::~VMHelper()
{
    Vix_ReleaseHandle(this->_vmHandle);
    VixHost_Disconnect(this->_hostHandle);
}


/*function check whether the VM completely booted
input: none
output: if vm booted
*/
bool VMHelper::isVmBooted()
{
    bool exist = false;
    VixHandle job = VixVM_ListProcessesInGuest(this->_vmHandle, 0, NULL, NULL);
    VixError err = VixJob_Wait(job, VIX_PROPERTY_NONE);
    if (VIX_OK != err) {
        std::cout << "VixVM_ListProcessesInGuest failed - " << Vix_GetErrorText(err, NULL) << std::endl;
        throw std::exception();
    }

    int num = VixJob_GetNumProperties(job, VIX_PROPERTY_JOB_RESULT_ITEM_NAME);
    for (int i = 0; (i < num) && !exist; i++) {
        char* pname;

        err = VixJob_GetNthProperties(job, i,
            VIX_PROPERTY_JOB_RESULT_ITEM_NAME, &pname,
            VIX_PROPERTY_NONE);

        if (std::string(pname) == "vm3dservice.exe")
            exist = true;

        Vix_FreeBuffer(pname);
    }

    Vix_ReleaseHandle(job);
    return exist;
}


/*function turn on vm
input: none
output: if it worked
*/
bool VMHelper::powerOn()
{
    VixError err;
    VixHandle jobHandle = VIX_INVALID_HANDLE;

    // Connect to the VM
    jobHandle = VixHost_Connect(VIX_API_VERSION,
        CONNTYPE,
        HOSTNAME, // *hostName,
        HOSTPORT, // hostPort,
        USERNAME, // *userName,
        PASSWORD, // *password,
        0, // options,
        VIX_INVALID_HANDLE, // propertyListHandle,
        NULL, // *callbackProc,
        NULL); // *clientData);
    err = VixJob_Wait(jobHandle,
        VIX_PROPERTY_JOB_RESULT_HANDLE,
        &this->_hostHandle,
        VIX_PROPERTY_NONE);
    Vix_ReleaseHandle(jobHandle);
    if (VIX_FAILED(err)) {
        std::cout << "VixHost_Connect failed - " << Vix_GetErrorText(err, NULL) << std::endl;
        return false;
    }

    // Open the VM
    jobHandle = VixVM_Open(this->_hostHandle,
        this->_vmxPath,
        NULL, // VixEventProc *callbackProc,
        NULL); // void *clientData);
    err = VixJob_Wait(jobHandle,
        VIX_PROPERTY_JOB_RESULT_HANDLE,
        &this->_vmHandle,
        VIX_PROPERTY_NONE);
    Vix_ReleaseHandle(jobHandle);
    if (VIX_FAILED(err)) {
        std::cout << "VixVM_Open failed - " << Vix_GetErrorText(err, NULL) << std::endl;
        return false;
    }
    
    // Power on the VM
    jobHandle = VixVM_PowerOn(this->_vmHandle,
        VMPOWEROPTIONS,
        VIX_INVALID_HANDLE,
        NULL, // *callbackProc,
        NULL); // *clientData);
    err = VixJob_Wait(jobHandle, VIX_PROPERTY_NONE);
    Vix_ReleaseHandle(jobHandle);
    if (VIX_FAILED(err)) {
        std::cout << "VixVM_PowerOn failed - " << Vix_GetErrorText(err, NULL) << std::endl;
        return false;
    }

    return this->LoginToVm();
}

/*
    Function login user to vmware
    Notes for VixVM_LoginInGuest function:
        + The password parameter can't be empty ("")
        + Before calling this function the vmware tools should be installed
            (there are functions for that but they are not recommended - Too much time, so it should be installed manually)
*/
bool VMHelper::LoginToVm()
{
    // Authenticate for guest operations for the first time
    VixHandle jobHandle = VixVM_LoginInGuest(this->_vmHandle,
        USERNAME, // userName
        PASSWORD, // password
        VIX_LOGIN_IN_GUEST_REQUIRE_INTERACTIVE_ENVIRONMENT, // options
        NULL, // callbackProc
        NULL); // clientData
    VixError err = VixJob_Wait(jobHandle, VIX_PROPERTY_NONE);
    Vix_ReleaseHandle(jobHandle);
    if (VIX_OK != err) 
    { // The machine is not yet completely ready
        // Login with the default flag for the check of processes running on the vm
        jobHandle = VixVM_LoginInGuest(this->_vmHandle,
            USERNAME, // userName
            PASSWORD, // password
            0,        // options
            NULL,     // callbackProc
            NULL);    // clientData
        err = VixJob_Wait(jobHandle, VIX_PROPERTY_NONE);
        Vix_ReleaseHandle(jobHandle);
        if (VIX_OK != err) {
            std::cout << "VixVM_LoginInGuest failed - " << Vix_GetErrorText(err, NULL) << std::endl;
            return false;
        }

        // checks if the vm is ready for login with flag: VIX_LOGIN_IN_GUEST_REQUIRE_INTERACTIVE_ENVIRONMENT
        try
        {
            while (!this->isVmBooted())
            {
                Sleep(3000); // wait 3 seconds
            }
        }
        catch (...)
        {
            return false;
        }

        // logout to login again
        jobHandle = VixVM_LogoutFromGuest(this->_vmHandle,
            NULL, // callbackProc
            NULL); // clientData
        err = VixJob_Wait(jobHandle, VIX_PROPERTY_NONE);
        Vix_ReleaseHandle(jobHandle);
        if (VIX_OK != err) {
            std::cout << "VixVM_LogoutFromGuest failed - " << Vix_GetErrorText(err, NULL) << std::endl;
            return false;
        }

        // Authenticate for guest operations again
        jobHandle = VixVM_LoginInGuest(this->_vmHandle,
            USERNAME, // userName
            PASSWORD, // password
            VIX_LOGIN_IN_GUEST_REQUIRE_INTERACTIVE_ENVIRONMENT, // options
            NULL, // callbackProc
            NULL); // clientData
        err = VixJob_Wait(jobHandle, VIX_PROPERTY_NONE);
        Vix_ReleaseHandle(jobHandle);
        if (VIX_OK != err) {
            std::cout << "VixVM_LoginInGuest failed - " << Vix_GetErrorText(err, NULL) << std::endl;
            return false;
        }
    }

    return true;
}


/*function turn off vm
input: none
output: if it worked
*/
bool VMHelper::powerOff()
{
    VixError err;
    VixHandle jobHandle;

    jobHandle = VixVM_PowerOff(this->_vmHandle,
    VIX_VMPOWEROP_NORMAL,
    NULL, // *callbackProc,
    NULL); // *clientData);

    err = VixJob_Wait(jobHandle, VIX_PROPERTY_NONE);
    Vix_ReleaseHandle(jobHandle);
    if (VIX_FAILED(err)) {
        std::cout << "VixVM_PowerOff failed - " << Vix_GetErrorText(err, NULL) << std::endl;
        return false;
    }

    return true;
}


/*function runs script on vm
input: none
output: if it worked
*/
bool VMHelper::runScript(const char* scriptText)
{
    VixError err;
    // Run a script
    VixHandle jobHandle = VixVM_RunScriptInGuest(this->_vmHandle,
        NULL,
        scriptText,
        0, // options,
        VIX_INVALID_HANDLE, // propertyListHandle,
        NULL, // callbackProc,
        NULL); // clientData
    err = VixJob_Wait(jobHandle, VIX_PROPERTY_NONE);

    if (VIX_OK != err) {
        std::cout << "VixVM_RunScriptInGuest failed - " << Vix_GetErrorText(err, NULL) << std::endl;
        return false;
    }
 
    Vix_ReleaseHandle(jobHandle);
    return true;
}


/*function runs the injector on vm as admin
input: path of the injector and parameters to its main
output: if it worked
*/
bool VMHelper::runInjector(const char* path, const char* argv)
{
    VixError err;

    // for administrator:
    std::string argpath = "/C " + std::string(path) + " " + std::string(argv);
    VixHandle jobHandle = VixVM_RunProgramInGuest(this->_vmHandle,
        "C:\\windows\\system32\\cmd.exe", // command
        argpath.c_str(), // commandline args
        VIX_RUNPROGRAM_ACTIVATE_WINDOW, // options,
        VIX_INVALID_HANDLE, // propertyListHandle,
        NULL, // callbackProc,
        NULL); // clientData

    err = VixJob_Wait(jobHandle, VIX_PROPERTY_NONE);
    Vix_ReleaseHandle(jobHandle);
    if (VIX_OK != err) {
        std::cout << "VixVM_RunProgramInGuest failed - " << Vix_GetErrorText(err, NULL) << std::endl;
        return false;
    }
    return true;
}

/*function runs a program on vm (in usermode)
input: path of the program and parameters to its main (no parameters by default)
output: if it worked
*/
bool VMHelper::runProgram(const char* path, const char* argv)
{
    VixError err;
     
    // Run the program
    VixHandle jobHandle = VixVM_RunProgramInGuest(this->_vmHandle,
        path, // command
        argv, // command line args
        VIX_RUNPROGRAM_ACTIVATE_WINDOW, // options,
        VIX_INVALID_HANDLE, // propertyListHandle,
        NULL, // callbackProc,
        NULL); // clientData

    err = VixJob_Wait(jobHandle, VIX_PROPERTY_NONE);
    Vix_ReleaseHandle(jobHandle);
    if (VIX_OK != err) {
        std::cout << "VixVM_RunProgramInGuest failed - " << Vix_GetErrorText(err, NULL) << std::endl;
        return false;
    }   
    return true;
}


/*function delete a folder from the VM
input: the path of the file to be deleted
output: none
*/
bool VMHelper::deleteDirectoryFromVm(const char* path)
{
    VixHandle jobHandle;
    VixError err;

    jobHandle = VixVM_DeleteDirectoryInGuest(this->_vmHandle,
        path,  // src path
        0,     // options
        NULL,  // callbackProc
        NULL); // clientData

    err = VixJob_Wait(jobHandle, VIX_PROPERTY_NONE);
    Vix_ReleaseHandle(jobHandle);
    if (VIX_OK != err) {
        std::cout << "VixVM_DeleteDirectoryInGuest failed - " << Vix_GetErrorText(err, NULL) << std::endl;
        return false;
    }

    return true;
}


/*function copies a file from host to guest (vm)
input: host - source path of file from pc, guest - destination path of file from vm (include the name of the file)
output: none
*/
bool VMHelper::copyFileFromPCToVM(const char* host, const char* guest)
{
    VixHandle jobHandle;
    VixError err;

    // Note: the paths can't contain unicode characters
    // Copy the file.
    jobHandle = VixVM_CopyFileFromHostToGuest(this->_vmHandle,
        host,  // src name
        guest, // dest name
        0, // options
        VIX_INVALID_HANDLE, // propertyListHandle
        NULL, // callbackProc
        NULL); // clientData
    err = VixJob_Wait(jobHandle, VIX_PROPERTY_NONE);
    Vix_ReleaseHandle(jobHandle);
    if (VIX_OK != err) {
        std::cout << "VixVM_CopyFileFromHostToGuest failed - " << Vix_GetErrorText(err, NULL) << std::endl;
        return false;
    }
  
    return true;
}


/*function copies a file from guest (vm) to this machine
input: host - destination path of file from pc (include the name of the file), guest - source path of file from vm
output: none
*/
bool VMHelper::copyFileFromVMToPC(const char* host, const char* guest)
{
    VixHandle jobHandle;
    VixError err;

    // Note: the paths can't contain unicode characters
    // Copy the file.
    jobHandle = VixVM_CopyFileFromGuestToHost(this->_vmHandle,
        guest,  // src name
        host, // dest name
        0, // options
        VIX_INVALID_HANDLE, // propertyListHandle
        NULL, // callbackProc
        NULL); // clientData
    err = VixJob_Wait(jobHandle, VIX_PROPERTY_NONE);
    Vix_ReleaseHandle(jobHandle);
    if (VIX_OK != err) {
        std::cout << "VixVM_CopyFileFromGuestToHost failed - " << Vix_GetErrorText(err, NULL) << std::endl;
        return false;
    }

    return true;
}