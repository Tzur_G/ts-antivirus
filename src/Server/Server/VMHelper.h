#pragma once
#include <iostream>
#include <Windows.h>
#include <fstream>

#include "vix.h"

#define USE_WORKSTATION

#ifdef USE_WORKSTATION

#define  CONNTYPE    VIX_SERVICEPROVIDER_VMWARE_WORKSTATION

#define  HOSTNAME ""
#define  HOSTPORT 0
#define  USERNAME "user"
#define  PASSWORD "pass"

#define  VMPOWEROPTIONS   VIX_VMPOWEROP_LAUNCH_GUI   // Launches the VMware Workstaion UI
// when powering on the virtual machine.

#else    // USE_WORKSTATION

/*
 * For VMware Server 2.0
 */

#define CONNTYPE VIX_SERVICEPROVIDER_VMWARE_VI_SERVER

#define HOSTNAME "https://192.2.3.4:8333/sdk"
 /*
  * NOTE: HOSTPORT is ignored, so the port should be specified as part
  * of the URL.
  */
#define HOSTPORT 0
#define USERNAME "root"
#define PASSWORD "hideme"

#define  VMPOWEROPTIONS VIX_VMPOWEROP_NORMAL

#endif    // USE_WORKSTATION

using std::cout;
using std::cin;
using std::endl;

class VMHelper {
private:
	char* _vmxPath;
	VixHandle _hostHandle;
	VixHandle _vmHandle;

public:
	VMHelper(char* vmxPath);
	~VMHelper();

	bool isVmBooted();
	bool powerOn();
	bool LoginToVm();
	bool powerOff();
	bool runScript(const char* scriptText);
	bool runInjector(const char* path, const char* argv);
	bool runProgram(const char* path, const char* argv = 0);
	bool copyFileFromPCToVM(const char* host, const char* guest);
	bool copyFileFromVMToPC(const char* host, const char* guest);
	bool deleteDirectoryFromVm(const char* path);
};
